 /* Modernizr 2.7.0 */
;window.Modernizr=function(a,b,c){function C(a){j.cssText=a}function D(a,b){return C(n.join(a+";")+(b||""))}function E(a,b){return typeof a===b}function F(a,b){return!!~(""+a).indexOf(b)}function G(a,b){for(var d in a){var e=a[d];if(!F(e,"-")&&j[e]!==c)return b=="pfx"?e:!0}return!1}function H(a,b,d){for(var e in a){var f=b[a[e]];if(f!==c)return d===!1?a[e]:E(f,"function")?f.bind(d||b):f}return!1}function I(a,b,c){var d=a.charAt(0).toUpperCase()+a.slice(1),e=(a+" "+p.join(d+" ")+d).split(" ");return E(b,"string")||E(b,"undefined")?G(e,b):(e=(a+" "+q.join(d+" ")+d).split(" "),H(e,b,c))}function J(){e.input=function(c){for(var d=0,e=c.length;d<e;d++)u[c[d]]=c[d]in k;return u.list&&(u.list=!!b.createElement("datalist")&&!!a.HTMLDataListElement),u}("autocomplete autofocus list placeholder max min multiple pattern required step".split(" ")),e.inputtypes=function(a){for(var d=0,e,f,h,i=a.length;d<i;d++)k.setAttribute("type",f=a[d]),e=k.type!=="text",e&&(k.value=l,k.style.cssText="position:absolute;visibility:hidden;",/^range$/.test(f)&&k.style.WebkitAppearance!==c?(g.appendChild(k),h=b.defaultView,e=h.getComputedStyle&&h.getComputedStyle(k,null).WebkitAppearance!=="textfield"&&k.offsetHeight!==0,g.removeChild(k)):/^(search|tel)$/.test(f)||(/^(url|email)$/.test(f)?e=k.checkValidity&&k.checkValidity()===!1:e=k.value!=l)),t[a[d]]=!!e;return t}("search tel url email datetime date month week time datetime-local number range color".split(" "))}var d="2.7.0",e={},f=!0,g=b.documentElement,h="modernizr",i=b.createElement(h),j=i.style,k=b.createElement("input"),l=":)",m={}.toString,n=" -webkit- -moz- -o- -ms- ".split(" "),o="Webkit Moz O ms",p=o.split(" "),q=o.toLowerCase().split(" "),r={svg:"http://www.w3.org/2000/svg"},s={},t={},u={},v=[],w=v.slice,x,y=function(a,c,d,e){var f,i,j,k,l=b.createElement("div"),m=b.body,n=m||b.createElement("body");if(parseInt(d,10))while(d--)j=b.createElement("div"),j.id=e?e[d]:h+(d+1),l.appendChild(j);return f=["&#173;",'<style id="s',h,'">',a,"</style>"].join(""),l.id=h,(m?l:n).innerHTML+=f,n.appendChild(l),m||(n.style.background="",n.style.overflow="hidden",k=g.style.overflow,g.style.overflow="hidden",g.appendChild(n)),i=c(l,a),m?l.parentNode.removeChild(l):(n.parentNode.removeChild(n),g.style.overflow=k),!!i},z=function(){function d(d,e){e=e||b.createElement(a[d]||"div"),d="on"+d;var f=d in e;return f||(e.setAttribute||(e=b.createElement("div")),e.setAttribute&&e.removeAttribute&&(e.setAttribute(d,""),f=E(e[d],"function"),E(e[d],"undefined")||(e[d]=c),e.removeAttribute(d))),e=null,f}var a={select:"input",change:"input",submit:"form",reset:"form",error:"img",load:"img",abort:"img"};return d}(),A={}.hasOwnProperty,B;!E(A,"undefined")&&!E(A.call,"undefined")?B=function(a,b){return A.call(a,b)}:B=function(a,b){return b in a&&E(a.constructor.prototype[b],"undefined")},Function.prototype.bind||(Function.prototype.bind=function(b){var c=this;if(typeof c!="function")throw new TypeError;var d=w.call(arguments,1),e=function(){if(this instanceof e){var a=function(){};a.prototype=c.prototype;var f=new a,g=c.apply(f,d.concat(w.call(arguments)));return Object(g)===g?g:f}return c.apply(b,d.concat(w.call(arguments)))};return e}),s.flexbox=function(){return I("flexWrap")},s.canvas=function(){var a=b.createElement("canvas");return!!a.getContext&&!!a.getContext("2d")},s.canvastext=function(){return!!e.canvas&&!!E(b.createElement("canvas").getContext("2d").fillText,"function")},s.webgl=function(){return!!a.WebGLRenderingContext},s.touch=function(){var c;return"ontouchstart"in a||a.DocumentTouch&&b instanceof DocumentTouch?c=!0:y(["@media (",n.join("touch-enabled),("),h,")","{#modernizr{top:9px;position:absolute}}"].join(""),function(a){c=a.offsetTop===9}),c},s.geolocation=function(){return"geolocation"in navigator},s.postmessage=function(){return!!a.postMessage},s.websqldatabase=function(){return!!a.openDatabase},s.indexedDB=function(){return!!I("indexedDB",a)},s.hashchange=function(){return z("hashchange",a)&&(b.documentMode===c||b.documentMode>7)},s.history=function(){return!!a.history&&!!history.pushState},s.draganddrop=function(){var a=b.createElement("div");return"draggable"in a||"ondragstart"in a&&"ondrop"in a},s.websockets=function(){return"WebSocket"in a||"MozWebSocket"in a},s.rgba=function(){return C("background-color:rgba(150,255,150,.5)"),F(j.backgroundColor,"rgba")},s.hsla=function(){return C("background-color:hsla(120,40%,100%,.5)"),F(j.backgroundColor,"rgba")||F(j.backgroundColor,"hsla")},s.multiplebgs=function(){return C("background:url(https://),url(https://),red url(https://)"),/(url\s*\(.*?){3}/.test(j.background)},s.backgroundsize=function(){return I("backgroundSize")},s.borderimage=function(){return I("borderImage")},s.borderradius=function(){return I("borderRadius")},s.boxshadow=function(){return I("boxShadow")},s.textshadow=function(){return b.createElement("div").style.textShadow===""},s.opacity=function(){return D("opacity:.55"),/^0.55$/.test(j.opacity)},s.cssanimations=function(){return I("animationName")},s.csscolumns=function(){return I("columnCount")},s.cssgradients=function(){var a="background-image:",b="gradient(linear,left top,right bottom,from(#9f9),to(white));",c="linear-gradient(left top,#9f9, white);";return C((a+"-webkit- ".split(" ").join(b+a)+n.join(c+a)).slice(0,-a.length)),F(j.backgroundImage,"gradient")},s.cssreflections=function(){return I("boxReflect")},s.csstransforms=function(){return!!I("transform")},s.csstransforms3d=function(){var a=!!I("perspective");return a&&"webkitPerspective"in g.style&&y("@media (transform-3d),(-webkit-transform-3d){#modernizr{left:9px;position:absolute;height:3px;}}",function(b,c){a=b.offsetLeft===9&&b.offsetHeight===3}),a},s.csstransitions=function(){return I("transition")},s.fontface=function(){var a;return y('@font-face {font-family:"font";src:url("https://")}',function(c,d){var e=b.getElementById("smodernizr"),f=e.sheet||e.styleSheet,g=f?f.cssRules&&f.cssRules[0]?f.cssRules[0].cssText:f.cssText||"":"";a=/src/i.test(g)&&g.indexOf(d.split(" ")[0])===0}),a},s.generatedcontent=function(){var a;return y(["#",h,"{font:0/0 a}#",h,':after{content:"',l,'";visibility:hidden;font:3px/1 a}'].join(""),function(b){a=b.offsetHeight>=3}),a},s.video=function(){var a=b.createElement("video"),c=!1;try{if(c=!!a.canPlayType)c=new Boolean(c),c.ogg=a.canPlayType('video/ogg; codecs="theora"').replace(/^no$/,""),c.h264=a.canPlayType('video/mp4; codecs="avc1.42E01E"').replace(/^no$/,""),c.webm=a.canPlayType('video/webm; codecs="vp8, vorbis"').replace(/^no$/,"")}catch(d){}return c},s.audio=function(){var a=b.createElement("audio"),c=!1;try{if(c=!!a.canPlayType)c=new Boolean(c),c.ogg=a.canPlayType('audio/ogg; codecs="vorbis"').replace(/^no$/,""),c.mp3=a.canPlayType("audio/mpeg;").replace(/^no$/,""),c.wav=a.canPlayType('audio/wav; codecs="1"').replace(/^no$/,""),c.m4a=(a.canPlayType("audio/x-m4a;")||a.canPlayType("audio/aac;")).replace(/^no$/,"")}catch(d){}return c},s.localstorage=function(){try{return localStorage.setItem(h,h),localStorage.removeItem(h),!0}catch(a){return!1}},s.sessionstorage=function(){try{return sessionStorage.setItem(h,h),sessionStorage.removeItem(h),!0}catch(a){return!1}},s.webworkers=function(){return!!a.Worker},s.applicationcache=function(){return!!a.applicationCache},s.svg=function(){return!!b.createElementNS&&!!b.createElementNS(r.svg,"svg").createSVGRect},s.inlinesvg=function(){var a=b.createElement("div");return a.innerHTML="<svg/>",(a.firstChild&&a.firstChild.namespaceURI)==r.svg},s.smil=function(){return!!b.createElementNS&&/SVGAnimate/.test(m.call(b.createElementNS(r.svg,"animate")))},s.svgclippaths=function(){return!!b.createElementNS&&/SVGClipPath/.test(m.call(b.createElementNS(r.svg,"clipPath")))};for(var K in s)B(s,K)&&(x=K.toLowerCase(),e[x]=s[K](),v.push((e[x]?"":"no-")+x));return e.input||J(),e.addTest=function(a,b){if(typeof a=="object")for(var d in a)B(a,d)&&e.addTest(d,a[d]);else{a=a.toLowerCase();if(e[a]!==c)return e;b=typeof b=="function"?b():b,typeof f!="undefined"&&f&&(g.className+=" "+(b?"":"no-")+a),e[a]=b}return e},C(""),i=k=null,function(a,b){function l(a,b){var c=a.createElement("p"),d=a.getElementsByTagName("head")[0]||a.documentElement;return c.innerHTML="x<style>"+b+"</style>",d.insertBefore(c.lastChild,d.firstChild)}function m(){var a=s.elements;return typeof a=="string"?a.split(" "):a}function n(a){var b=j[a[h]];return b||(b={},i++,a[h]=i,j[i]=b),b}function o(a,c,d){c||(c=b);if(k)return c.createElement(a);d||(d=n(c));var g;return d.cache[a]?g=d.cache[a].cloneNode():f.test(a)?g=(d.cache[a]=d.createElem(a)).cloneNode():g=d.createElem(a),g.canHaveChildren&&!e.test(a)&&!g.tagUrn?d.frag.appendChild(g):g}function p(a,c){a||(a=b);if(k)return a.createDocumentFragment();c=c||n(a);var d=c.frag.cloneNode(),e=0,f=m(),g=f.length;for(;e<g;e++)d.createElement(f[e]);return d}function q(a,b){b.cache||(b.cache={},b.createElem=a.createElement,b.createFrag=a.createDocumentFragment,b.frag=b.createFrag()),a.createElement=function(c){return s.shivMethods?o(c,a,b):b.createElem(c)},a.createDocumentFragment=Function("h,f","return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&("+m().join().replace(/[\w\-]+/g,function(a){return b.createElem(a),b.frag.createElement(a),'c("'+a+'")'})+");return n}")(s,b.frag)}function r(a){a||(a=b);var c=n(a);return s.shivCSS&&!g&&!c.hasCSS&&(c.hasCSS=!!l(a,"article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}")),k||q(a,c),a}var c="3.7.0",d=a.html5||{},e=/^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,f=/^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,g,h="_html5shiv",i=0,j={},k;(function(){try{var a=b.createElement("a");a.innerHTML="<xyz></xyz>",g="hidden"in a,k=a.childNodes.length==1||function(){b.createElement("a");var a=b.createDocumentFragment();return typeof a.cloneNode=="undefined"||typeof a.createDocumentFragment=="undefined"||typeof a.createElement=="undefined"}()}catch(c){g=!0,k=!0}})();var s={elements:d.elements||"abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",version:c,shivCSS:d.shivCSS!==!1,supportsUnknownElements:k,shivMethods:d.shivMethods!==!1,type:"default",shivDocument:r,createElement:o,createDocumentFragment:p};a.html5=s,r(b)}(this,b),e._version=d,e._prefixes=n,e._domPrefixes=q,e._cssomPrefixes=p,e.hasEvent=z,e.testProp=function(a){return G([a])},e.testAllProps=I,e.testStyles=y,e.prefixed=function(a,b,c){return b?I(a,b,c):I(a,"pfx")},g.className=g.className.replace(/(^|\s)no-js(\s|$)/,"$1$2")+(f?" js "+v.join(" "):""),e}(this,this.document),function(a,b,c){function d(a){return"[object Function]"==o.call(a)}function e(a){return"string"==typeof a}function f(){}function g(a){return!a||"loaded"==a||"complete"==a||"uninitialized"==a}function h(){var a=p.shift();q=1,a?a.t?m(function(){("c"==a.t?B.injectCss:B.injectJs)(a.s,0,a.a,a.x,a.e,1)},0):(a(),h()):q=0}function i(a,c,d,e,f,i,j){function k(b){if(!o&&g(l.readyState)&&(u.r=o=1,!q&&h(),l.onload=l.onreadystatechange=null,b)){"img"!=a&&m(function(){t.removeChild(l)},50);for(var d in y[c])y[c].hasOwnProperty(d)&&y[c][d].onload()}}var j=j||B.errorTimeout,l=b.createElement(a),o=0,r=0,u={t:d,s:c,e:f,a:i,x:j};1===y[c]&&(r=1,y[c]=[]),"object"==a?l.data=c:(l.src=c,l.type=a),l.width=l.height="0",l.onerror=l.onload=l.onreadystatechange=function(){k.call(this,r)},p.splice(e,0,u),"img"!=a&&(r||2===y[c]?(t.insertBefore(l,s?null:n),m(k,j)):y[c].push(l))}function j(a,b,c,d,f){return q=0,b=b||"j",e(a)?i("c"==b?v:u,a,b,this.i++,c,d,f):(p.splice(this.i++,0,a),1==p.length&&h()),this}function k(){var a=B;return a.loader={load:j,i:0},a}var l=b.documentElement,m=a.setTimeout,n=b.getElementsByTagName("script")[0],o={}.toString,p=[],q=0,r="MozAppearance"in l.style,s=r&&!!b.createRange().compareNode,t=s?l:n.parentNode,l=a.opera&&"[object Opera]"==o.call(a.opera),l=!!b.attachEvent&&!l,u=r?"object":l?"script":"img",v=l?"script":u,w=Array.isArray||function(a){return"[object Array]"==o.call(a)},x=[],y={},z={timeout:function(a,b){return b.length&&(a.timeout=b[0]),a}},A,B;B=function(a){function b(a){var a=a.split("!"),b=x.length,c=a.pop(),d=a.length,c={url:c,origUrl:c,prefixes:a},e,f,g;for(f=0;f<d;f++)g=a[f].split("="),(e=z[g.shift()])&&(c=e(c,g));for(f=0;f<b;f++)c=x[f](c);return c}function g(a,e,f,g,h){var i=b(a),j=i.autoCallback;i.url.split(".").pop().split("?").shift(),i.bypass||(e&&(e=d(e)?e:e[a]||e[g]||e[a.split("/").pop().split("?")[0]]),i.instead?i.instead(a,e,f,g,h):(y[i.url]?i.noexec=!0:y[i.url]=1,f.load(i.url,i.forceCSS||!i.forceJS&&"css"==i.url.split(".").pop().split("?").shift()?"c":c,i.noexec,i.attrs,i.timeout),(d(e)||d(j))&&f.load(function(){k(),e&&e(i.origUrl,h,g),j&&j(i.origUrl,h,g),y[i.url]=2})))}function h(a,b){function c(a,c){if(a){if(e(a))c||(j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}),g(a,j,b,0,h);else if(Object(a)===a)for(n in m=function(){var b=0,c;for(c in a)a.hasOwnProperty(c)&&b++;return b}(),a)a.hasOwnProperty(n)&&(!c&&!--m&&(d(j)?j=function(){var a=[].slice.call(arguments);k.apply(this,a),l()}:j[n]=function(a){return function(){var b=[].slice.call(arguments);a&&a.apply(this,b),l()}}(k[n])),g(a[n],j,b,n,h))}else!c&&l()}var h=!!a.test,i=a.load||a.both,j=a.callback||f,k=j,l=a.complete||f,m,n;c(h?a.yep:a.nope,!!i),i&&c(i)}var i,j,l=this.yepnope.loader;if(e(a))g(a,0,l,0);else if(w(a))for(i=0;i<a.length;i++)j=a[i],e(j)?g(j,0,l,0):w(j)?B(j):Object(j)===j&&h(j,l);else Object(a)===a&&h(a,l)},B.addPrefix=function(a,b){z[a]=b},B.addFilter=function(a){x.push(a)},B.errorTimeout=1e4,null==b.readyState&&b.addEventListener&&(b.readyState="loading",b.addEventListener("DOMContentLoaded",A=function(){b.removeEventListener("DOMContentLoaded",A,0),b.readyState="complete"},0)),a.yepnope=k(),a.yepnope.executeStack=h,a.yepnope.injectJs=function(a,c,d,e,i,j){var k=b.createElement("script"),l,o,e=e||B.errorTimeout;k.src=a;for(o in d)k.setAttribute(o,d[o]);c=j?h:c||f,k.onreadystatechange=k.onload=function(){!l&&g(k.readyState)&&(l=1,c(),k.onload=k.onreadystatechange=null)},m(function(){l||(l=1,c(1))},e),i?k.onload():n.parentNode.insertBefore(k,n)},a.yepnope.injectCss=function(a,c,d,e,g,i){var e=b.createElement("link"),j,c=i?h:c||f;e.href=a,e.rel="stylesheet",e.type="text/css";for(j in d)e.setAttribute(j,d[j]);g||(n.parentNode.insertBefore(e,n),m(c,0))}}(this,document),Modernizr.load=function(){yepnope.apply(window,[].slice.call(arguments,0))};
/*! * jScrollPane - v2.0.19mod */
(function(b,Y){var s=function(s){return b(s,Y)};"function"===typeof define&&define.amd?define(["jquery"],s):"object"===typeof exports?module.exports=s:s(jQuery)})(function(b,Y,s){b.fn.jScrollPane=function(C){function ga(c,C){function ha(a){var d,k,r,q,y,t=!1,m=!1;e=a;if(f===s)q=c.scrollTop(),y=c.scrollLeft(),c.css({padding:0}),l=c.innerWidth()+G,h=c.innerHeight(),c.width(l),f=b('<div class="jspPane" />').css("padding",ia).append(c.children()),g=b('<div class="jspContainer" />').css({width:l+"px",
height:h+"px"}).append(f).appendTo(c),f.wrap('<div style="overflow:hidden;height:100%;width:100%;position:relative;"></div>').wrapInner("<div></div>");else{c.css("width","");t=e.stickToBottom&&va();m=e.stickToRight&&wa();if(r=c.innerWidth()+G!=l||c.outerHeight()!=h)l=c.innerWidth()+G,h=c.innerHeight(),g.css({width:l+"px",height:h+"px"});if(!r&&oa==v&&f.outerHeight()==u){c.width(l);return}oa=v;f.css("width","");c.width(l);g.find(">.jspVerticalBar,>.jspHorizontalBar").remove().end()}f.css("overflow",
"auto");v=a.contentWidth?a.contentWidth:f[0].scrollWidth;u=f[0].scrollHeight;f.css("overflow","");ja=v/l;Z=u/h;z=1<Z;if((A=1<ja)||z){c.addClass("jspScrollable");if(a=e.maintainPosition&&(n||p))d=H(),k=I();ga();xa();ya();a&&(Q(m?v-l:d,!1),J(t?u-h:k,!1));za();Aa();Ba();e.enableKeyboardNavigation&&Ca();e.clickOnTrack&&Da();Ea();e.hijackInternalLinks&&Fa()}else c.removeClass("jspScrollable"),f.css({top:0,left:0,width:g.width()-G}),g.unbind(ka),f.find(":input,a").unbind("focus.jsp"),c.attr("tabindex",
"-1").removeAttr("tabindex").unbind("keydown.jsp keypress.jsp"),pa();e.autoReinitialise&&!R?R=setInterval(function(){ha(e)},e.autoReinitialiseDelay):!e.autoReinitialise&&R&&clearInterval(R);q&&c.scrollTop(0)&&J(q,!1);y&&c.scrollLeft(0)&&Q(y,!1);c.trigger("jsp-initialised",[A||z])}function ga(){z&&(g.append(b('<div class="jspVerticalBar" />').append(b('<div class="jspCap jspCapTop" />'),b('<div class="jspTrack" />').append(b('<div class="jspDrag" />').append(b('<div class="jspDragTop" />'),b('<div class="jspDragBottom" />'))),
b('<div class="jspCap jspCapBottom" />'))),$=g.find(">.jspVerticalBar"),D=$.find(">.jspTrack"),w=D.find(">.jspDrag"),e.showArrows&&(U=b('<a class="jspArrow jspArrowUp" />').bind("mousedown.jsp",K(0,-1)).bind("click.jsp",S),V=b('<a class="jspArrow jspArrowDown" />').bind("mousedown.jsp",K(0,1)).bind("click.jsp",S),e.arrowScrollOnHover&&(U.bind("mouseover.jsp",K(0,-1,U)),V.bind("mouseover.jsp",K(0,1,V))),qa(D,e.verticalArrowPositions,U,V)),L=h,g.find(">.jspVerticalBar>.jspCap:visible,>.jspVerticalBar>.jspArrow").each(function(){L-=
b(this).outerHeight()}),w.hover(function(){w.addClass("jspHover")},function(){w.removeClass("jspHover")}).bind("mousedown.jsp",function(a){b("html").bind("dragstart.jsp selectstart.jsp",S);w.addClass("jspActive");var d=a.pageY-w.position().top;b("html").bind("mousemove.jsp",function(a){P(a.pageY-d,!1)}).bind("mouseup.jsp mouseleave.jsp",ra);return!1}),sa())}function sa(){D.height(L+"px");n=0;ta=e.verticalGutter+D.outerWidth();f.width(l-G);try{0===$.position().left&&f.css("margin-left",ta+"px")}catch(a){}}
function xa(){A&&(g.append(b('<div class="jspHorizontalBar" />').append(b('<div class="jspCap jspCapLeft" />'),b('<div class="jspTrack" />').append(b('<div class="jspDrag" />').append(b('<div class="jspDragLeft" />'),b('<div class="jspDragRight" />'))),b('<div class="jspCap jspCapRight" />'))),aa=g.find(">.jspHorizontalBar"),E=aa.find(">.jspTrack"),x=E.find(">.jspDrag"),e.showArrows&&(W=b('<a class="jspArrow jspArrowLeft" />').bind("mousedown.jsp",K(-1,0)).bind("click.jsp",S),X=b('<a class="jspArrow jspArrowRight" />').bind("mousedown.jsp",
K(1,0)).bind("click.jsp",S),e.arrowScrollOnHover&&(W.bind("mouseover.jsp",K(-1,0,W)),X.bind("mouseover.jsp",K(1,0,X))),qa(E,e.horizontalArrowPositions,W,X)),x.hover(function(){x.addClass("jspHover")},function(){x.removeClass("jspHover")}).bind("mousedown.jsp",function(a){b("html").bind("dragstart.jsp selectstart.jsp",S);x.addClass("jspActive");var d=a.pageX-x.position().left;b("html").bind("mousemove.jsp",function(a){T(a.pageX-d,!1)}).bind("mouseup.jsp mouseleave.jsp",ra);return!1}),B=g.innerWidth(),
ua())}function ua(){g.find(">.jspHorizontalBar>.jspCap:visible,>.jspHorizontalBar>.jspArrow").each(function(){B-=b(this).outerWidth()});E.width(B+"px");p=0}function ya(){if(A&&z){var a=E.outerHeight(),d=D.outerWidth();L-=a;b(aa).find(">.jspCap:visible,>.jspArrow").each(function(){B+=b(this).outerWidth()});B-=d;h-=d;l-=a;E.parent().append(b('<div class="jspCorner" />').css("width",a+"px"));sa();ua()}A&&f.width(g.outerWidth()-G+"px");u=f.outerHeight();Z=u/h;A&&(M=Math.ceil(1/ja*B),M>e.horizontalDragMaxWidth?
M=e.horizontalDragMaxWidth:M<e.horizontalDragMinWidth&&(M=e.horizontalDragMinWidth),x.width(M+"px"),N=B-M,la(p));z&&(O=Math.ceil(1/Z*L),O>e.verticalDragMaxHeight?O=e.verticalDragMaxHeight:O<e.verticalDragMinHeight&&(O=e.verticalDragMinHeight),w.height(O+"px"),F=L-O,ma(n))}function qa(a,d,k,b){var e="before",c="after";"os"==d&&(d=/Mac/.test(navigator.platform)?"after":"split");d==e?c=d:d==c&&(e=d,d=k,k=b,b=d);a[e](k)[c](b)}function K(a,d,b){return function(){Ga(a,d,this,b);this.blur();return!1}}function Ga(a,
d,k,c){k=b(k).addClass("jspActive");var q,y,t=!0,f=function(){0!==a&&m.scrollByX(a*e.arrowButtonSpeed);0!==d&&m.scrollByY(d*e.arrowButtonSpeed);y=setTimeout(f,t?e.initialDelay:e.arrowRepeatFreq);t=!1};f();q=c?"mouseout.jsp":"mouseup.jsp";c=c||b("html");c.bind(q,function(){k.removeClass("jspActive");y&&clearTimeout(y);y=null;c.unbind(q)})}function Da(){pa();z&&D.bind("mousedown.jsp",function(a){if(a.originalTarget===s||a.originalTarget==a.currentTarget){var d=b(this),k=d.offset(),c=a.pageY-k.top-n,
q,y=!0,t=function(){var b=d.offset(),b=a.pageY-b.top-O/2,k=h*e.scrollPagePercent,g=F*k/(u-h);if(0>c)n-g>b?m.scrollByY(-k):P(b);else if(0<c)n+g<b?m.scrollByY(k):P(b);else{f();return}q=setTimeout(t,y?e.initialDelay:e.trackClickRepeatFreq);y=!1},f=function(){q&&clearTimeout(q);q=null;b(document).unbind("mouseup.jsp",f)};t();b(document).bind("mouseup.jsp",f);return!1}});A&&E.bind("mousedown.jsp",function(a){if(a.originalTarget===s||a.originalTarget==a.currentTarget){var d=b(this),k=d.offset(),c=a.pageX-
k.left-p,q,f=!0,t=function(){var b=d.offset(),b=a.pageX-b.left-M/2,k=l*e.scrollPagePercent,h=N*k/(v-l);if(0>c)p-h>b?m.scrollByX(-k):T(b);else if(0<c)p+h<b?m.scrollByX(k):T(b);else{g();return}q=setTimeout(t,f?e.initialDelay:e.trackClickRepeatFreq);f=!1},g=function(){q&&clearTimeout(q);q=null;b(document).unbind("mouseup.jsp",g)};t();b(document).bind("mouseup.jsp",g);return!1}})}function pa(){E&&E.unbind("mousedown.jsp");D&&D.unbind("mousedown.jsp")}function ra(){b("html").unbind("dragstart.jsp selectstart.jsp mousemove.jsp mouseup.jsp mouseleave.jsp");
w&&w.removeClass("jspActive");x&&x.removeClass("jspActive")}function P(a,d){z&&(0>a?a=0:a>F&&(a=F),d===s&&(d=e.animateScroll),d?m.animate(w,"top",a,ma):(w.css("top",a),ma(a)))}function ma(a){a===s&&(a=w.position().top);g.scrollTop(0);n=a;var d=0===n,b=n==F;a=-(a/F)*(u-h);if(ba!=d||ca!=b)ba=d,ca=b,c.trigger("jsp-arrow-change",[ba,ca,da,ea]);e.showArrows&&(U[d?"addClass":"removeClass"]("jspDisabled"),V[b?"addClass":"removeClass"]("jspDisabled"));f.css("top",a);c.trigger("jsp-scroll-y",[-a,d,b]).trigger("scroll")}
function T(a,d){A&&(0>a?a=0:a>N&&(a=N),d===s&&(d=e.animateScroll),d?m.animate(x,"left",a,la):(x.css("left",a),la(a)))}function la(a){a===s&&(a=x.position().left);g.scrollTop(0);p=a;var d=0===p,b=p==N;a=-(a/N)*(v-l);if(da!=d||ea!=b)da=d,ea=b,c.trigger("jsp-arrow-change",[ba,ca,da,ea]);e.showArrows&&(W[d?"addClass":"removeClass"]("jspDisabled"),X[b?"addClass":"removeClass"]("jspDisabled"));f.css("left",a);c.trigger("jsp-scroll-x",[-a,d,b]).trigger("scroll")}function J(a,d){P(a/(u-h)*F,d)}function Q(a,
d){T(a/(v-l)*N,d)}function fa(a,d,c){var r,q,f=0,t=0,m,n,p;try{r=b(a)}catch(u){return}q=r.outerHeight();a=r.outerWidth();g.scrollTop(0);for(g.scrollLeft(0);!r.is(".jspPane");)if(f+=r.position().top,t+=r.position().left,r=r.offsetParent(),/^body|html$/i.test(r[0].nodeName))return;r=I();m=r+h;f<r||d?n=f-e.horizontalGutter:f+q>m&&(n=f-h+q+e.horizontalGutter);isNaN(n)||J(n,c);f=H();n=f+l;t<f||d?p=t-e.horizontalGutter:t+a>n&&(p=t-l+a+e.horizontalGutter);isNaN(p)||Q(p,c)}function H(){return-f.position().left}
function I(){return-f.position().top}function va(){var a=u-h;return 20<a&&10>a-I()}function wa(){var a=v-l;return 20<a&&10>a-H()}function Aa(){g.unbind(ka).bind(ka,function(a,d,b,c){a=p;d=n;var f=e.mouseWheelSpeed;m.scrollBy(b*f,-c*f,!1);return a==p&&d==n})}function S(){return!1}function za(){f.find(":input,a").unbind("focus.jsp").bind("focus.jsp",function(a){fa(a.target,!1)})}function Ca(){function a(){var a=p,b=n;switch(d){case 40:m.scrollByY(e.keyboardSpeed,!1);break;case 38:m.scrollByY(-e.keyboardSpeed,
!1);break;case 34:case 32:m.scrollByY(h*e.scrollPagePercent,!1);break;case 33:m.scrollByY(-h*e.scrollPagePercent,!1);break;case 39:m.scrollByX(e.keyboardSpeed,!1);break;case 37:m.scrollByX(-e.keyboardSpeed,!1)}return k=a!=p||b!=n}var d,k,r=[];A&&r.push(aa[0]);z&&r.push($[0]);f.focus(function(){c.focus()});c.attr("tabindex",0).unbind("keydown.jsp keypress.jsp").bind("keydown.jsp",function(c){if(c.target===this||r.length&&b(c.target).closest(r).length){var e=p,f=n;switch(c.keyCode){case 40:case 38:case 34:case 32:case 33:case 39:case 37:d=
c.keyCode;a();break;case 35:J(u-h);d=null;break;case 36:J(0),d=null}k=c.keyCode==d&&e!=p||f!=n;return!k}}).bind("keypress.jsp",function(b){b.keyCode==d&&a();return!k});e.hideFocus?(c.css("outline","none"),"hideFocus"in g[0]&&c.attr("hideFocus",!0)):(c.css("outline",""),"hideFocus"in g[0]&&c.attr("hideFocus",!1))}function Ea(){if(location.hash&&1<location.hash.length){var a,d,c=escape(location.hash.substr(1));try{a=b("#"+c+', a[name="'+c+'"]')}catch(e){return}a.length&&f.find(c)&&(0===g.scrollTop()?
d=setInterval(function(){0<g.scrollTop()&&(fa(a,!0),b(document).scrollTop(g.position().top),clearInterval(d))},50):(fa(a,!0),b(document).scrollTop(g.position().top)))}}function Fa(){b(document.body).data("jspHijack")||(b(document.body).data("jspHijack",!0),b(document.body).delegate("a[href*=#]","click",function(a){var d=this.href.substr(0,this.href.indexOf("#")),c=location.href,e;-1!==location.href.indexOf("#")&&(c=location.href.substr(0,location.href.indexOf("#")));if(d===c){d=escape(this.href.substr(this.href.indexOf("#")+
1));try{e=b("#"+d+', a[name="'+d+'"]')}catch(f){return}e.length&&(d=e.closest(".jspScrollable"),c=d.data("jsp"),c.scrollToElement(e,!0),d[0].scrollIntoView&&(c=b(Y).scrollTop(),e=e.offset().top,(e<c||e>c+b(Y).height())&&d[0].scrollIntoView()),a.preventDefault())}}))}function Ba(){var a,d,b,c,e,f=!1;g.unbind("touchstart.jsp touchmove.jsp touchend.jsp click.jsp-touchclick").bind("touchstart.jsp",function(g){g=g.originalEvent.touches[0];a=H()/mobile_scale;d=I()/mobile_scale;b=g.pageX/mobile_scale;c=
g.pageY/mobile_scale;e=!1;f=!0}).bind("touchmove.jsp",function(g){if(f){g=g.originalEvent.touches[0];var h=p,l=n;m.scrollTo(a+b-g.pageX/mobile_scale,d+c-g.pageY/mobile_scale);e=e||5<Math.abs(b-g.pageX/mobile_scale)||5<Math.abs(c-g.pageY/mobile_scale);return h==p&&l==n}}).bind("touchend.jsp",function(a){f=!1}).bind("click.jsp-touchclick",function(a){if(e)return e=!1})}var e,m=this,f,l,h,g,v,u,ja,Z,z,A,w,F,n,x,N,p,$,D,ta,L,O,U,V,aa,E,B,M,W,X,R,ia,G,oa,ba=!0,da=!0,ca=!1,ea=!1,na=c.clone(!1,!1).empty(),
ka=b.fn.mwheelIntent?"mwheelIntent.jsp":"mousewheel.jsp";mobile_scale||(mobile_scale=1);"border-box"===c.css("box-sizing")?G=ia=0:(ia=c.css("paddingTop")+" "+c.css("paddingRight")+" "+c.css("paddingBottom")+" "+c.css("paddingLeft"),G=(parseInt(c.css("paddingLeft"),10)||0)+(parseInt(c.css("paddingRight"),10)||0));b.extend(m,{reinitialise:function(a){a=b.extend({},e,a);ha(a)},scrollToElement:function(a,d,b){fa(a,d,b)},scrollTo:function(a,d,b){Q(a,b);J(d,b)},scrollToX:function(a,b){Q(a,b)},scrollToY:function(a,
b){J(a,b)},scrollToPercentX:function(a,b){Q(a*(v-l),b)},scrollToPercentY:function(a,b){J(a*(u-h),b)},scrollBy:function(a,b,c){m.scrollByX(a,c);m.scrollByY(b,c)},scrollByX:function(a,b){var c=(H()+Math[0>a?"floor":"ceil"](a))/(v-l);T(c*N,b)},scrollByY:function(a,b){var c=(I()+Math[0>a?"floor":"ceil"](a))/(u-h);P(c*F,b)},positionDragX:function(a,b){T(a,b)},positionDragY:function(a,b){P(a,b)},animate:function(a,b,c,f){var g={};g[b]=c;a.animate(g,{duration:e.animateDuration,easing:e.animateEase,queue:!1,
step:f})},getContentPositionX:function(){return H()},getContentPositionY:function(){return I()},getContentWidth:function(){return v},getContentHeight:function(){return u},getPercentScrolledX:function(){return H()/(v-l)},getPercentScrolledY:function(){return I()/(u-h)},getIsScrollableH:function(){return A},getIsScrollableV:function(){return z},getContentPane:function(){return f},scrollToBottom:function(a){P(F,a)},hijackInternalLinks:b.noop,destroy:function(){var a=I(),b=H();c.removeClass("jspScrollable").unbind(".jsp");
c.replaceWith(na.append(f.children()));na.scrollTop(a);na.scrollLeft(b);R&&clearInterval(R)}});ha(C)}C=b.extend({},b.fn.jScrollPane.defaults,C);b.each(["arrowButtonSpeed","trackClickSpeed","keyboardSpeed"],function(){C[this]=C[this]||C.speed});return this.each(function(){var c=b(this),s=c.data("jsp");s?s.reinitialise(C):(b("script",c).filter('[type="text/javascript"],:not([type])').remove(),s=new ga(c,C),c.data("jsp",s))})};b.fn.jScrollPane.defaults={showArrows:!1,maintainPosition:!0,stickToBottom:!1,
stickToRight:!1,clickOnTrack:!0,autoReinitialise:!1,autoReinitialiseDelay:500,verticalDragMinHeight:0,verticalDragMaxHeight:99999,horizontalDragMinWidth:0,horizontalDragMaxWidth:99999,contentWidth:s,animateScroll:!1,animateDuration:300,animateEase:"linear",hijackInternalLinks:!1,verticalGutter:4,horizontalGutter:4,mouseWheelSpeed:30,arrowButtonSpeed:0,arrowRepeatFreq:50,arrowScrollOnHover:!1,trackClickSpeed:0,trackClickRepeatFreq:70,verticalArrowPositions:"split",horizontalArrowPositions:"split",
enableKeyboardNavigation:!0,hideFocus:!1,keyboardSpeed:0,initialDelay:300,speed:30,scrollPagePercent:.8}},this);


/* pjax2 v0.68 by cooltyn*/
(function($){
	jQuery.fn.pjax2=function(options){
		options=$.extend({
			beforeSend: function() {
			},
			success: function() {				
			},
			error: function() {
				load_content('/page404');
			},
			success_function: function() {					
			}
		},options);
		function get_pathname() {
			return (window.location.pathname+window.location.hash).replace(/#!?/,'/').replace(/(.)\/$/,'$1').replace(/\/{2,}/g,'/');
		}
		$.support.pjax=window.history && window.history.pushState && window.history.replaceState && 1==1;		
		if($.support.pjax && window.location.hash!='') {
			window.location.replace(get_pathname());
		} else {
			var frame=this;
			var prev_page=get_pathname();
			var pages_cache={};
			var ajax_first_load=false;
			if(!$.support.pjax) {
				ajax_first_load=true;	
			}
			function change_url(url,options) {
				options=$.extend({},options);
				if(options.suppress_load) {
					prev_page=url;
				}
				if($.support.pjax) {
					load_content(url);
					history.pushState(url, null, url);
				} else {
					//window.location.hash='#!'+url.replace(/^\/en($|\/)/,'/').slice(1);
					window.location.hash='#!'+url.slice(1);
				}
				
			}
			function success_load(url,page,data) {
				window.document.title=/to_title">(.*)<\/div>/.exec(data)[1];
				var type=/to_type">(.*)<\/div>/.exec(data)[1];
				var custom_type=/to_custom_type">(.*)<\/div>/.exec(data)[1];
				options.success(data,type,url,page,custom_type);
			}
			function load_content(url) {
				//console.log(url,prev_page)
				if(url && (url!=prev_page || ajax_first_load)) {
					ajax_first_load=false;
					var page=/\/([\w-]*)$/.exec(url)[1];
					prev_page=url;
					if(page=='') {
						page='home';
					}
					if(pages_cache[url]) {
						options.beforeSend(page);
						success_load(url,page,pages_cache[url]);
					} else {
						$.ajax({
							type: "GET",
							cache: false,
							url: url,
							beforeSend: function(request){
								options.beforeSend(page);
								request.setRequestHeader('X-PJAX','true');
							},
							success: function(data){
								success_load(url,page,data);								
								pages_cache[url]=data;
							},
							error: options.error
						})
					}
				}
			}
			frame.load_content=function(url,options){
				change_url(url,options);
			}
			if($.support.pjax) {
				history.replaceState(prev_page, null);
				window.addEventListener('popstate', function(e){
					load_content(e.state);
				}, false);
				options.success_function($('body').attr('class'),null);
			} else {
				var real_pathname=window.location.pathname;
				if(real_pathname!='/') {
					window.location.replace('/#!'+prev_page.slice(1));
				}
				setTimeout(function(){
					$.router(function() {
						load_content(get_pathname(),true);
					})
				},0)
			}
			$(document).on('click','a.pjax',function(){
				change_url($(this).attr('href'));
				return false;
			})
		}
	};
})(jQuery);


 
/* jQuery Transit mod - 0.9.12 */
;(function(a,b){if(typeof define==="function"&&define.amd){define(["jquery"],b)}else{if(typeof exports==="object"){module.exports=b(require("jquery"))}else{b(a.jQuery)}}}(this,function(k){k.transit={version:"0.9.12",propertyMap:{marginLeft:"margin",marginRight:"margin",marginBottom:"margin",marginTop:"margin",paddingLeft:"padding",paddingRight:"padding",paddingBottom:"padding",paddingTop:"padding"},enabled:true,useTransitionEnd:false};var d=document.createElement("div");var q={};function b(v){if(v in d.style){return v}var u=["Moz","Webkit","O","ms"];var r=v.charAt(0).toUpperCase()+v.substr(1);for(var t=0;t<u.length;++t){var s=u[t]+r;if(s in d.style){return s}}}function e(){d.style[q.transform]="";d.style[q.transform]="rotateY(90deg)";return d.style[q.transform]!==""}var a=navigator.userAgent.toLowerCase().indexOf("chrome")>-1;q.transition=b("transition");q.transitionProperty=b("transitionProperty");q.transitionDelay=b("transitionDelay");q.transform=b("transform");q.transformOrigin=b("transformOrigin");q.filter=b("Filter");q.transform3d=e();var i={transition:"transitionend",MozTransition:"transitionend",OTransition:"oTransitionEnd",WebkitTransition:"webkitTransitionEnd",msTransition:"MSTransitionEnd"};var f=q.transitionEnd=i[q.transition]||null;for(var p in q){if(q.hasOwnProperty(p)&&typeof k.support[p]==="undefined"){k.support[p]=q[p]}}d=null;k.cssEase={_default:"ease","in":"ease-in",out:"ease-out","in-out":"ease-in-out",snap:"cubic-bezier(0,1,.5,1)",easeInCubic:"cubic-bezier(.550,.055,.675,.190)",easeOutCubic:"cubic-bezier(.215,.61,.355,1)",easeInOutCubic:"cubic-bezier(.645,.045,.355,1)",easeInCirc:"cubic-bezier(.6,.04,.98,.335)",easeOutCirc:"cubic-bezier(.075,.82,.165,1)",easeInOutCirc:"cubic-bezier(.785,.135,.15,.86)",easeInExpo:"cubic-bezier(.95,.05,.795,.035)",easeOutExpo:"cubic-bezier(.19,1,.22,1)",easeInOutExpo:"cubic-bezier(1,0,0,1)",easeInQuad:"cubic-bezier(.55,.085,.68,.53)",easeOutQuad:"cubic-bezier(.25,.46,.45,.94)",easeInOutQuad:"cubic-bezier(.455,.03,.515,.955)",easeInQuart:"cubic-bezier(.895,.03,.685,.22)",easeOutQuart:"cubic-bezier(.165,.84,.44,1)",easeInOutQuart:"cubic-bezier(.77,0,.175,1)",easeInQuint:"cubic-bezier(.755,.05,.855,.06)",easeOutQuint:"cubic-bezier(.23,1,.32,1)",easeInOutQuint:"cubic-bezier(.86,0,.07,1)",easeInSine:"cubic-bezier(.47,0,.745,.715)",easeOutSine:"cubic-bezier(.39,.575,.565,1)",easeInOutSine:"cubic-bezier(.445,.05,.55,.95)",easeInBack:"cubic-bezier(.6,-.28,.735,.045)",easeOutBack:"cubic-bezier(.175, .885,.32,1.275)",easeInOutBack:"cubic-bezier(.68,-.55,.265,1.55)"};k.cssHooks["transit:transform"]={get:function(r){return k(r).data("transform")||new j()},set:function(s,r){var t=r;if(!(t instanceof j)){t=new j(t)}if(q.transform==="WebkitTransform"&&!a){s.style[q.transform]=t.toString(true)}else{s.style[q.transform]=t.toString()}k(s).data("transform",t)}};k.cssHooks.transform={set:k.cssHooks["transit:transform"].set};k.cssHooks.filter={get:function(r){return r.style[q.filter]},set:function(r,s){r.style[q.filter]=s}};if(k.fn.jquery<"1.8"){k.cssHooks.transformOrigin={get:function(r){return r.style[q.transformOrigin]},set:function(r,s){r.style[q.transformOrigin]=s}};k.cssHooks.transition={get:function(r){return r.style[q.transition]},set:function(r,s){r.style[q.transition]=s}}}n("scale");n("scaleX");n("scaleY");n("translate");n("translate3d");n("rotate");n("rotateX");n("rotateY");n("rotate3d");n("perspective");n("skewX");n("skewY");n("x",true);n("y",true);function j(r){if(typeof r==="string"){this.parse(r)}return this}j.prototype={setFromString:function(t,s){var r=(typeof s==="string")?s.split(","):(s.constructor===Array)?s:[s];r.unshift(t);j.prototype.set.apply(this,r)},set:function(s){var r=Array.prototype.slice.apply(arguments,[1]);if(this.setter[s]){this.setter[s].apply(this,r)}else{this[s]=r.join(",")}},get:function(r){if(this.getter[r]){return this.getter[r].apply(this)}else{return this[r]||0}},setter:{rotate:function(r){this.rotate=o(r,"deg")},rotateX:function(r){this.rotateX=o(r,"deg")},rotateY:function(r){this.rotateY=o(r,"deg")},scale:function(r,s){if(s===undefined){s=r}this.scale=r+","+s},skewX:function(r){this.skewX=o(r,"deg")},skewY:function(r){this.skewY=o(r,"deg")},perspective:function(r){this.perspective=o(r,"px")},x:function(r){this.set("translate",r,null)},y:function(r){this.set("translate",null,r)},translate:function(r,s){if(this._translateX===undefined){this._translateX=0}if(this._translateY===undefined){this._translateY=0}if(r!==null&&r!==undefined){this._translateX=o(r,"px")}if(s!==null&&s!==undefined){this._translateY=o(s,"px")}this.translate=this._translateX+","+this._translateY},translate3d:function(r,t,s){if(this._translateX===undefined){this._translateX=0}if(this._translateY===undefined){this._translateY=0}if(this._translateZ===undefined){this._translateZ=0}if(r!==null&&r!==undefined){this._translateX=o(r,"px")}if(t!==null&&t!==undefined){this._translateY=o(t,"px")}if(s!==null&&s!==undefined){this._translateZ=o(s,"px")}this.translate3d=this._translateX+","+this._translateY+","+this._translateZ}},getter:{x:function(){return this._translateX||0},y:function(){return this._translateY||0},scale:function(){var r=(this.scale||"1,1").split(",");if(r[0]){r[0]=parseFloat(r[0])}if(r[1]){r[1]=parseFloat(r[1])}return(r[0]===r[1])?r[0]:r},rotate3d:function(){var t=(this.rotate3d||"0,0,0,0deg").split(",");for(var r=0;r<=3;++r){if(t[r]){t[r]=parseFloat(t[r])}}if(t[3]){t[3]=o(t[3],"deg")}return t}},parse:function(s){var r=this;s.replace(/([a-zA-Z0-9]+)\((.*?)\)/g,function(t,v,u){r.setFromString(v,u)})},toString:function(t){var s=[];for(var r in this){if(this.hasOwnProperty(r)){if((!q.transform3d)&&((r==="rotateX")||(r==="rotateY")||(r==="perspective")||(r==="transformOrigin"))){continue}if(r[0]!=="_"){if(t&&(r==="scale")){s.push(r+"3d("+this[r]+",1)")}else{if(t&&(r==="translate")){s.push(r+"3d("+this[r]+",0)")}else{s.push(r+"("+this[r]+")")}}}}}return s.join(" ")}};function m(s,r,t){if(r===true){s.queue(t)}else{if(r){s.queue(r,t)}else{s.each(function(){t.call(this)})}}}function h(s){var r=[];k.each(s,function(t){t=k.camelCase(t);t=k.transit.propertyMap[t]||k.cssProps[t]||t;t=c(t);if(q[t]){t=c(q[t])}if(k.inArray(t,r)===-1){r.push(t)}});return r}function g(s,v,x,r){var t=h(s);if(k.cssEase[x]){x=k.cssEase[x]}var w=""+l(v)+" "+x;if(parseInt(r,10)>0){w+=" "+l(r)}var u=[];k.each(t,function(z,y){u.push(y+" "+w)});return u.join(", ")}k.fn.transition=k.fn.transit=function(A,t,z,D){var E=this;var v=0;var x=true;var r=k.extend(true,{},A);if(typeof t==="function"){D=t;t=undefined}if(typeof t==="object"){z=t.easing;v=t.delay||0;x=typeof t.queue==="undefined"?true:t.queue;D=t.complete;t=t.duration}if(typeof z==="function"){D=z;z=undefined}if(typeof r.easing!=="undefined"){z=r.easing;delete r.easing}if(typeof r.duration!=="undefined"){t=r.duration;delete r.duration}if(typeof r.complete!=="undefined"){D=r.complete;delete r.complete}if(typeof r.queue!=="undefined"){x=r.queue;delete r.queue}if(typeof r.delay!=="undefined"){v=r.delay;delete r.delay}if(typeof t==="undefined"){t=k.fx.speeds._default}if(typeof z==="undefined"){z=k.cssEase._default}t=l(t);var F=g(r,t,z,v);var C=k.transit.enabled&&q.transition;var u=C?(parseInt(t,10)+parseInt(v,10)):0;if(u===0){var B=function(G){E.css(r);if(D){D.apply(E)}if(G){G()}};m(E,x,B);return E}var y={};var s=function(J){var H=false;var G=function(){if(H){E.unbind(f,G)}if(u>0){E.each(function(){this.style[q.transition]=(y[this]||null)})}if(typeof D==="function"){D.apply(E)}if(typeof J==="function"){J()}};if((u>0)&&(f)&&(k.transit.useTransitionEnd)){H=true;E.bind(f,G)}else{var I=window.setTimeout(G,u);E.data("transitTimer",I)}E.each(function(){if(u>0){this.style[q.transition]=F}k(this).css(r)});E.data("transitCallback",G)};var w=function(G){this.offsetWidth;s(G)};m(E,x,w);return this};k.fn.transitionStop=k.fn.transitStop=function(r,s){this.each(function(){var u=k(this);var z=u.data("transitTimer");clearTimeout(z);u.data("transitTimer",null);var x=this.style[q.transitionProperty];if(x){x=x.replace(/\s*/g,"").split(",");var y=window.getComputedStyle(this),w={};for(var v=0;v<x.length;v++){w[x[v]]=this.style[x[v]];this.style[x[v]]=y[x[v]]}this.offsetWidth;this.style[q.transition]="none";if(s){for(var v=0;v<x.length;v++){this.style[x[v]]=w[x[v]]}var t=u.data("transitCallback");if(typeof t==="function"){t()}u.data("transitCallback",null)}else{if(r){u.clearQueue();u.unbind(f)}else{u.dequeue()}}}});return this};function n(s,r){if(!r){k.cssNumber[s]=true}k.transit.propertyMap[s]=q.transform;k.cssHooks[s]={get:function(v){var u=k(v).css("transit:transform");return u.get(s)},set:function(v,w){var u=k(v).css("transit:transform");u.setFromString(s,w);k(v).css({"transit:transform":u})}}}function c(r){return r.replace(/([A-Z])/g,function(s){return"-"+s.toLowerCase()})}function o(s,r){if((typeof s==="string")&&(!s.match(/^[\-0-9\.]+$/))){return s}else{return""+s+r}}function l(s){var r=s;if(typeof r==="string"&&(!r.match(/^[\-0-9\.]+/))){r=k.fx.speeds[r]||k.fx.speeds._default}return o(r,"ms")}k.transit.getTransitionValue=g;return k}));
/*! Video.js v4.6.3 Copyright 2014 Brightcove, Inc. https://github.com/videojs/video.js/blob/master/LICENSE */ 
(function() {var b=void 0,f=!0,j=null,l=!1;function m(){return function(){}}function q(a){return function(){return this[a]}}function r(a){return function(){return a}}var t;document.createElement("video");document.createElement("audio");document.createElement("track");function u(a,c,d){if("string"===typeof a){0===a.indexOf("#")&&(a=a.slice(1));if(u.Aa[a])return u.Aa[a];a=u.w(a)}if(!a||!a.nodeName)throw new TypeError("The element or ID supplied is not valid. (videojs)");return a.player||new u.Player(a,c,d)}
var videojs=u;window.ke=window.le=u;u.Vb="4.6";u.Qc="https:"==document.location.protocol?"https://":"http://";u.options={techOrder:["html5","flash"],html5:{},flash:{},width:300,height:150,defaultVolume:0,playbackRates:[],children:{mediaLoader:{},posterImage:{},textTrackDisplay:{},loadingSpinner:{},bigPlayButton:{},controlBar:{},errorDisplay:{}},notSupportedMessage:"No compatible source was found for this video."};"GENERATED_CDN_VSN"!==u.Vb&&(videojs.options.flash.swf=u.Qc+"vjs.zencdn.net/"+u.Vb+"/video-js.swf");
u.Aa={};"function"===typeof define&&define.amd?define([],function(){return videojs}):"object"===typeof exports&&"object"===typeof module&&(module.exports=videojs);u.pa=u.CoreObject=m();u.pa.extend=function(a){var c,d;a=a||{};c=a.init||a.h||this.prototype.init||this.prototype.h||m();d=function(){c.apply(this,arguments)};d.prototype=u.l.create(this.prototype);d.prototype.constructor=d;d.extend=u.pa.extend;d.create=u.pa.create;for(var e in a)a.hasOwnProperty(e)&&(d.prototype[e]=a[e]);return d};
u.pa.create=function(){var a=u.l.create(this.prototype);this.apply(a,arguments);return a};u.d=function(a,c,d){var e=u.getData(a);e.D||(e.D={});e.D[c]||(e.D[c]=[]);d.v||(d.v=u.v++);e.D[c].push(d);e.X||(e.disabled=l,e.X=function(c){if(!e.disabled){c=u.pc(c);var d=e.D[c.type];if(d)for(var d=d.slice(0),k=0,p=d.length;k<p&&!c.xc();k++)d[k].call(a,c)}});1==e.D[c].length&&(document.addEventListener?a.addEventListener(c,e.X,l):document.attachEvent&&a.attachEvent("on"+c,e.X))};
u.p=function(a,c,d){if(u.tc(a)){var e=u.getData(a);if(e.D)if(c){var g=e.D[c];if(g){if(d){if(d.v)for(e=0;e<g.length;e++)g[e].v===d.v&&g.splice(e--,1)}else e.D[c]=[];u.kc(a,c)}}else for(g in e.D)c=g,e.D[c]=[],u.kc(a,c)}};u.kc=function(a,c){var d=u.getData(a);0===d.D[c].length&&(delete d.D[c],document.removeEventListener?a.removeEventListener(c,d.X,l):document.detachEvent&&a.detachEvent("on"+c,d.X));u.Fb(d.D)&&(delete d.D,delete d.X,delete d.disabled);u.Fb(d)&&u.Ec(a)};
u.pc=function(a){function c(){return f}function d(){return l}if(!a||!a.Gb){var e=a||window.event;a={};for(var g in e)"layerX"!==g&&("layerY"!==g&&"keyboardEvent.keyLocation"!==g)&&("returnValue"==g&&e.preventDefault||(a[g]=e[g]));a.target||(a.target=a.srcElement||document);a.relatedTarget=a.fromElement===a.target?a.toElement:a.fromElement;a.preventDefault=function(){e.preventDefault&&e.preventDefault();a.returnValue=l;a.sd=c;a.defaultPrevented=f};a.sd=d;a.defaultPrevented=l;a.stopPropagation=function(){e.stopPropagation&&
e.stopPropagation();a.cancelBubble=f;a.Gb=c};a.Gb=d;a.stopImmediatePropagation=function(){e.stopImmediatePropagation&&e.stopImmediatePropagation();a.xc=c;a.stopPropagation()};a.xc=d;if(a.clientX!=j){g=document.documentElement;var h=document.body;a.pageX=a.clientX+(g&&g.scrollLeft||h&&h.scrollLeft||0)-(g&&g.clientLeft||h&&h.clientLeft||0);a.pageY=a.clientY+(g&&g.scrollTop||h&&h.scrollTop||0)-(g&&g.clientTop||h&&h.clientTop||0)}a.which=a.charCode||a.keyCode;a.button!=j&&(a.button=a.button&1?0:a.button&
4?1:a.button&2?2:0)}return a};u.k=function(a,c){var d=u.tc(a)?u.getData(a):{},e=a.parentNode||a.ownerDocument;"string"===typeof c&&(c={type:c,target:a});c=u.pc(c);d.X&&d.X.call(a,c);if(e&&!c.Gb()&&c.bubbles!==l)u.k(e,c);else if(!e&&!c.defaultPrevented&&(d=u.getData(c.target),c.target[c.type])){d.disabled=f;if("function"===typeof c.target[c.type])c.target[c.type]();d.disabled=l}return!c.defaultPrevented};
u.W=function(a,c,d){function e(){u.p(a,c,e);d.apply(this,arguments)}e.v=d.v=d.v||u.v++;u.d(a,c,e)};var v=Object.prototype.hasOwnProperty;u.e=function(a,c){var d,e;d=document.createElement(a||"div");for(e in c)v.call(c,e)&&(-1!==e.indexOf("aria-")||"role"==e?d.setAttribute(e,c[e]):d[e]=c[e]);return d};u.$=function(a){return a.charAt(0).toUpperCase()+a.slice(1)};u.l={};u.l.create=Object.create||function(a){function c(){}c.prototype=a;return new c};
u.l.wa=function(a,c,d){for(var e in a)v.call(a,e)&&c.call(d||this,e,a[e])};u.l.B=function(a,c){if(!c)return a;for(var d in c)v.call(c,d)&&(a[d]=c[d]);return a};u.l.gd=function(a,c){var d,e,g;a=u.l.copy(a);for(d in c)v.call(c,d)&&(e=a[d],g=c[d],a[d]=u.l.Sa(e)&&u.l.Sa(g)?u.l.gd(e,g):c[d]);return a};u.l.copy=function(a){return u.l.B({},a)};u.l.Sa=function(a){return!!a&&"object"===typeof a&&"[object Object]"===a.toString()&&a.constructor===Object};
u.bind=function(a,c,d){function e(){return c.apply(a,arguments)}c.v||(c.v=u.v++);e.v=d?d+"_"+c.v:c.v;return e};u.ta={};u.v=1;u.expando="vdata"+(new Date).getTime();u.getData=function(a){var c=a[u.expando];c||(c=a[u.expando]=u.v++,u.ta[c]={});return u.ta[c]};u.tc=function(a){a=a[u.expando];return!(!a||u.Fb(u.ta[a]))};u.Ec=function(a){var c=a[u.expando];if(c){delete u.ta[c];try{delete a[u.expando]}catch(d){a.removeAttribute?a.removeAttribute(u.expando):a[u.expando]=j}}};
u.Fb=function(a){for(var c in a)if(a[c]!==j)return l;return f};u.o=function(a,c){-1==(" "+a.className+" ").indexOf(" "+c+" ")&&(a.className=""===a.className?c:a.className+" "+c)};u.r=function(a,c){var d,e;if(-1!=a.className.indexOf(c)){d=a.className.split(" ");for(e=d.length-1;0<=e;e--)d[e]===c&&d.splice(e,1);a.className=d.join(" ")}};u.A=u.e("video");u.M=navigator.userAgent;u.Vc=/iPhone/i.test(u.M);u.Uc=/iPad/i.test(u.M);u.Wc=/iPod/i.test(u.M);u.Tc=u.Vc||u.Uc||u.Wc;var aa=u,w;var x=u.M.match(/OS (\d+)_/i);
w=x&&x[1]?x[1]:b;aa.$d=w;u.Sc=/Android/i.test(u.M);var ba=u,y;var z=u.M.match(/Android (\d+)(?:\.(\d+))?(?:\.(\d+))*/i),A,B;z?(A=z[1]&&parseFloat(z[1]),B=z[2]&&parseFloat(z[2]),y=A&&B?parseFloat(z[1]+"."+z[2]):A?A:j):y=j;ba.Ub=y;u.Xc=u.Sc&&/webkit/i.test(u.M)&&2.3>u.Ub;u.Yb=/Firefox/i.test(u.M);u.ae=/Chrome/i.test(u.M);u.fc=!!("ontouchstart"in window||window.Rc&&document instanceof window.Rc);
u.Cb=function(a){var c,d,e,g;c={};if(a&&a.attributes&&0<a.attributes.length){d=a.attributes;for(var h=d.length-1;0<=h;h--){e=d[h].name;g=d[h].value;if("boolean"===typeof a[e]||-1!==",autoplay,controls,loop,muted,default,".indexOf(","+e+","))g=g!==j?f:l;c[e]=g}}return c};
u.de=function(a,c){var d="";document.defaultView&&document.defaultView.getComputedStyle?d=document.defaultView.getComputedStyle(a,"").getPropertyValue(c):a.currentStyle&&(d=a["client"+c.substr(0,1).toUpperCase()+c.substr(1)]+"px");return d};u.Eb=function(a,c){c.firstChild?c.insertBefore(a,c.firstChild):c.appendChild(a)};u.Na={};u.w=function(a){0===a.indexOf("#")&&(a=a.slice(1));return document.getElementById(a)};
u.ya=function(a,c){c=c||a;var d=Math.floor(a%60),e=Math.floor(a/60%60),g=Math.floor(a/3600),h=Math.floor(c/60%60),k=Math.floor(c/3600);if(isNaN(a)||Infinity===a)g=e=d="-";g=0<g||0<k?g+":":"";return g+(((g||10<=h)&&10>e?"0"+e:e)+":")+(10>d?"0"+d:d)};u.cd=function(){document.body.focus();document.onselectstart=r(l)};u.Ud=function(){document.onselectstart=r(f)};u.trim=function(a){return(a+"").replace(/^\s+|\s+$/g,"")};u.round=function(a,c){c||(c=0);return Math.round(a*Math.pow(10,c))/Math.pow(10,c)};
u.zb=function(a,c){return{length:1,start:function(){return a},end:function(){return c}}};
u.get=function(a,c,d,e){var g,h,k,p;d=d||m();"undefined"===typeof XMLHttpRequest&&(window.XMLHttpRequest=function(){try{return new window.ActiveXObject("Msxml2.XMLHTTP.6.0")}catch(a){}try{return new window.ActiveXObject("Msxml2.XMLHTTP.3.0")}catch(c){}try{return new window.ActiveXObject("Msxml2.XMLHTTP")}catch(d){}throw Error("This browser does not support XMLHttpRequest.");});h=new XMLHttpRequest;k=u.Gd(a);p=window.location;k.protocol+k.host!==p.protocol+p.host&&window.XDomainRequest&&!("withCredentials"in
h)?(h=new window.XDomainRequest,h.onload=function(){c(h.responseText)},h.onerror=d,h.onprogress=m(),h.ontimeout=d):(g="file:"==k.protocol||"file:"==p.protocol,h.onreadystatechange=function(){4===h.readyState&&(200===h.status||g&&0===h.status?c(h.responseText):d(h.responseText))});try{h.open("GET",a,f),e&&(h.withCredentials=f)}catch(n){d(n);return}try{h.send()}catch(s){d(s)}};
u.Ld=function(a){try{var c=window.localStorage||l;c&&(c.volume=a)}catch(d){22==d.code||1014==d.code?u.log("LocalStorage Full (VideoJS)",d):18==d.code?u.log("LocalStorage not allowed (VideoJS)",d):u.log("LocalStorage Error (VideoJS)",d)}};u.rc=function(a){a.match(/^https?:\/\//)||(a=u.e("div",{innerHTML:'<a href="'+a+'">x</a>'}).firstChild.href);return a};
u.Gd=function(a){var c,d,e,g;g="protocol hostname port pathname search hash host".split(" ");d=u.e("a",{href:a});if(e=""===d.host&&"file:"!==d.protocol)c=u.e("div"),c.innerHTML='<a href="'+a+'"></a>',d=c.firstChild,c.setAttribute("style","display:none; position:absolute;"),document.body.appendChild(c);a={};for(var h=0;h<g.length;h++)a[g[h]]=d[g[h]];e&&document.body.removeChild(c);return a};function D(){}var E=window.console||{log:D,warn:D,error:D};
function F(a,c){var d=Array.prototype.slice.call(c);a?d.unshift(a.toUpperCase()+":"):a="log";u.log.history.push(d);d.unshift("VIDEOJS:");if(E[a].apply)E[a].apply(E,d);else E[a](d.join(" "))}u.log=function(){F(j,arguments)};u.log.history=[];u.log.error=function(){};u.log.warn=function(){F("warn",arguments)};
u.pd=function(a){var c,d;a.getBoundingClientRect&&a.parentNode&&(c=a.getBoundingClientRect());if(!c)return{left:0,top:0};a=document.documentElement;d=document.body;return{left:u.round(c.left+(window.pageXOffset||d.scrollLeft)-(a.clientLeft||d.clientLeft||0)),top:u.round(c.top+(window.pageYOffset||d.scrollTop)-(a.clientTop||d.clientTop||0))}};u.oa={};u.oa.Kb=function(a,c){var d,e,g;a=u.l.copy(a);for(d in c)c.hasOwnProperty(d)&&(e=a[d],g=c[d],a[d]=u.l.Sa(e)&&u.l.Sa(g)?u.oa.Kb(e,g):c[d]);return a};
u.a=u.pa.extend({h:function(a,c,d){this.c=a;this.j=u.l.copy(this.j);c=this.options(c);this.T=c.id||(c.el&&c.el.id?c.el.id:a.id()+"_component_"+u.v++);this.xd=c.name||j;this.b=c.el||this.e();this.N=[];this.Oa={};this.Pa={};this.vc();this.K(d);if(c.Fc!==l){var e,g;e=u.bind(this.m(),this.m().reportUserActivity);this.d("touchstart",function(){e();clearInterval(g);g=setInterval(e,250)});a=function(){e();clearInterval(g)};this.d("touchmove",e);this.d("touchend",a);this.d("touchcancel",a)}}});t=u.a.prototype;
t.dispose=function(){this.k({type:"dispose",bubbles:l});if(this.N)for(var a=this.N.length-1;0<=a;a--)this.N[a].dispose&&this.N[a].dispose();this.Pa=this.Oa=this.N=j;this.p();this.b.parentNode&&this.b.parentNode.removeChild(this.b);u.Ec(this.b);this.b=j};t.c=f;t.m=q("c");t.options=function(a){return a===b?this.j:this.j=u.oa.Kb(this.j,a)};t.e=function(a,c){return u.e(a,c)};t.w=q("b");t.ia=function(){return this.u||this.b};t.id=q("T");t.name=q("xd");t.children=q("N");t.rd=function(a){return this.Oa[a]};
t.ja=function(a){return this.Pa[a]};t.V=function(a,c){var d,e;"string"===typeof a?(e=a,c=c||{},d=c.componentClass||u.$(e),c.name=e,d=new window.videojs[d](this.c||this,c)):d=a;this.N.push(d);"function"===typeof d.id&&(this.Oa[d.id()]=d);(e=e||d.name&&d.name())&&(this.Pa[e]=d);"function"===typeof d.el&&d.el()&&this.ia().appendChild(d.el());return d};
t.removeChild=function(a){"string"===typeof a&&(a=this.ja(a));if(a&&this.N){for(var c=l,d=this.N.length-1;0<=d;d--)if(this.N[d]===a){c=f;this.N.splice(d,1);break}c&&(this.Oa[a.id]=j,this.Pa[a.name]=j,(c=a.w())&&c.parentNode===this.ia()&&this.ia().removeChild(a.w()))}};t.vc=function(){var a,c,d,e;a=this;if(c=this.options().children)if(c instanceof Array)for(var g=0;g<c.length;g++)d=c[g],"string"==typeof d?(e=d,d={}):e=d.name,a[e]=a.V(e,d);else u.l.wa(c,function(c,d){d!==l&&(a[c]=a.V(c,d))})};t.S=r("");
t.d=function(a,c){u.d(this.b,a,u.bind(this,c));return this};t.p=function(a,c){u.p(this.b,a,c);return this};t.W=function(a,c){u.W(this.b,a,u.bind(this,c));return this};t.k=function(a,c){u.k(this.b,a,c);return this};t.K=function(a){a&&(this.ca?a.call(this):(this.Za===b&&(this.Za=[]),this.Za.push(a)));return this};t.Ea=function(){this.ca=f;var a=this.Za;if(a&&0<a.length){for(var c=0,d=a.length;c<d;c++)a[c].call(this);this.Za=[];this.k("ready")}};t.o=function(a){u.o(this.b,a);return this};
t.r=function(a){u.r(this.b,a);return this};t.show=function(){this.b.style.display="block";return this};t.G=function(){this.b.style.display="none";return this};function G(a){a.r("vjs-lock-showing")}t.disable=function(){this.G();this.show=m()};t.width=function(a,c){return H(this,"width",a,c)};t.height=function(a,c){return H(this,"height",a,c)};t.kd=function(a,c){return this.width(a,f).height(c)};
function H(a,c,d,e){if(d!==b)return a.b.style[c]=-1!==(""+d).indexOf("%")||-1!==(""+d).indexOf("px")?d:"auto"===d?"":d+"px",e||a.k("resize"),a;if(!a.b)return 0;d=a.b.style[c];e=d.indexOf("px");return-1!==e?parseInt(d.slice(0,e),10):parseInt(a.b["offset"+u.$(c)],10)}
function I(a){var c,d,e,g,h,k,p,n;c=0;d=j;a.d("touchstart",function(a){1===a.touches.length&&(d=a.touches[0],c=(new Date).getTime(),g=f)});a.d("touchmove",function(a){1<a.touches.length?g=l:d&&(k=a.touches[0].pageX-d.pageX,p=a.touches[0].pageY-d.pageY,n=Math.sqrt(k*k+p*p),22<n&&(g=l))});h=function(){g=l};a.d("touchleave",h);a.d("touchcancel",h);a.d("touchend",function(a){d=j;g===f&&(e=(new Date).getTime()-c,250>e&&(a.preventDefault(),this.k("tap")))})}
u.s=u.a.extend({h:function(a,c){u.a.call(this,a,c);I(this);this.d("tap",this.q);this.d("click",this.q);this.d("focus",this.Va);this.d("blur",this.Ua)}});t=u.s.prototype;
t.e=function(a,c){var d;c=u.l.B({className:this.S(),role:"button","aria-live":"polite",tabIndex:0},c);d=u.a.prototype.e.call(this,a,c);c.innerHTML||(this.u=u.e("div",{className:"vjs-control-content"}),this.xb=u.e("span",{className:"vjs-control-text",innerHTML:this.sa||"Need Text"}),this.u.appendChild(this.xb),d.appendChild(this.u));return d};t.S=function(){return"vjs-control "+u.a.prototype.S.call(this)};t.q=m();t.Va=function(){u.d(document,"keyup",u.bind(this,this.da))};
t.da=function(a){if(32==a.which||13==a.which)a.preventDefault(),this.q()};t.Ua=function(){u.p(document,"keyup",u.bind(this,this.da))};u.Q=u.a.extend({h:function(a,c){u.a.call(this,a,c);this.bd=this.ja(this.j.barName);this.handle=this.ja(this.j.handleName);this.d("mousedown",this.Wa);this.d("touchstart",this.Wa);this.d("focus",this.Va);this.d("blur",this.Ua);this.d("click",this.q);this.c.d("controlsvisible",u.bind(this,this.update));a.d(this.Bc,u.bind(this,this.update));this.R={}}});t=u.Q.prototype;
t.e=function(a,c){c=c||{};c.className+=" vjs-slider";c=u.l.B({role:"slider","aria-valuenow":0,"aria-valuemin":0,"aria-valuemax":100,tabIndex:0},c);return u.a.prototype.e.call(this,a,c)};t.Wa=function(a){a.preventDefault();u.cd();this.R.move=u.bind(this,this.Lb);this.R.end=u.bind(this,this.Mb);u.d(document,"mousemove",this.R.move);u.d(document,"mouseup",this.R.end);u.d(document,"touchmove",this.R.move);u.d(document,"touchend",this.R.end);this.Lb(a)};
t.Mb=function(){u.Ud();u.p(document,"mousemove",this.R.move,l);u.p(document,"mouseup",this.R.end,l);u.p(document,"touchmove",this.R.move,l);u.p(document,"touchend",this.R.end,l);this.update()};t.update=function(){if(this.b){var a,c=this.Db(),d=this.handle,e=this.bd;isNaN(c)&&(c=0);a=c;if(d){a=this.b.offsetWidth;var g=d.w().offsetWidth;a=g?g/a:0;c*=1-a;a=c+a/2;d.w().style.left=u.round(100*c,2)+"%"}e.w().style.width=u.round(100*a,2)+"%"}};
function J(a,c){var d,e,g,h;d=a.b;e=u.pd(d);h=g=d.offsetWidth;d=a.handle;if(a.j.Wd)return h=e.top,e=c.changedTouches?c.changedTouches[0].pageY:c.pageY,d&&(d=d.w().offsetHeight,h+=d/2,g-=d),Math.max(0,Math.min(1,(h-e+g)/g));g=e.left;e=c.changedTouches?c.changedTouches[0].pageX:c.pageX;d&&(d=d.w().offsetWidth,g+=d/2,h-=d);return Math.max(0,Math.min(1,(e-g)/h))}t.Va=function(){u.d(document,"keyup",u.bind(this,this.da))};
t.da=function(a){37==a.which?(a.preventDefault(),this.Hc()):39==a.which&&(a.preventDefault(),this.Ic())};t.Ua=function(){u.p(document,"keyup",u.bind(this,this.da))};t.q=function(a){a.stopImmediatePropagation();a.preventDefault()};u.Y=u.a.extend();u.Y.prototype.defaultValue=0;u.Y.prototype.e=function(a,c){c=c||{};c.className+=" vjs-slider-handle";c=u.l.B({innerHTML:'<span class="vjs-control-text">'+this.defaultValue+"</span>"},c);return u.a.prototype.e.call(this,"div",c)};u.ga=u.a.extend();
function ca(a,c){a.V(c);c.d("click",u.bind(a,function(){G(this)}))}u.ga.prototype.e=function(){var a=this.options().lc||"ul";this.u=u.e(a,{className:"vjs-menu-content"});a=u.a.prototype.e.call(this,"div",{append:this.u,className:"vjs-menu"});a.appendChild(this.u);u.d(a,"click",function(a){a.preventDefault();a.stopImmediatePropagation()});return a};u.I=u.s.extend({h:function(a,c){u.s.call(this,a,c);this.selected(c.selected)}});
u.I.prototype.e=function(a,c){return u.s.prototype.e.call(this,"li",u.l.B({className:"vjs-menu-item",innerHTML:this.j.label},c))};u.I.prototype.q=function(){this.selected(f)};u.I.prototype.selected=function(a){a?(this.o("vjs-selected"),this.b.setAttribute("aria-selected",f)):(this.r("vjs-selected"),this.b.setAttribute("aria-selected",l))};
u.L=u.s.extend({h:function(a,c){u.s.call(this,a,c);this.za=this.va();this.V(this.za);this.O&&0===this.O.length&&this.G();this.d("keyup",this.da);this.b.setAttribute("aria-haspopup",f);this.b.setAttribute("role","button")}});t=u.L.prototype;t.ra=l;t.va=function(){var a=new u.ga(this.c);this.options().title&&a.ia().appendChild(u.e("li",{className:"vjs-menu-title",innerHTML:u.$(this.options().title),Sd:-1}));if(this.O=this.createItems())for(var c=0;c<this.O.length;c++)ca(a,this.O[c]);return a};
t.ua=m();t.S=function(){return this.className+" vjs-menu-button "+u.s.prototype.S.call(this)};t.Va=m();t.Ua=m();t.q=function(){this.W("mouseout",u.bind(this,function(){G(this.za);this.b.blur()}));this.ra?K(this):L(this)};t.da=function(a){a.preventDefault();32==a.which||13==a.which?this.ra?K(this):L(this):27==a.which&&this.ra&&K(this)};function L(a){a.ra=f;a.za.o("vjs-lock-showing");a.b.setAttribute("aria-pressed",f);a.O&&0<a.O.length&&a.O[0].w().focus()}
function K(a){a.ra=l;G(a.za);a.b.setAttribute("aria-pressed",l)}u.F=function(a){"number"===typeof a?this.code=a:"string"===typeof a?this.message=a:"object"===typeof a&&u.l.B(this,a);this.message||(this.message=u.F.hd[this.code]||"")};u.F.prototype.code=0;u.F.prototype.message="";u.F.prototype.status=j;u.F.Ra="MEDIA_ERR_CUSTOM MEDIA_ERR_ABORTED MEDIA_ERR_NETWORK MEDIA_ERR_DECODE MEDIA_ERR_SRC_NOT_SUPPORTED MEDIA_ERR_ENCRYPTED".split(" ");
u.F.hd={1:"You aborted the video playback",2:"A network error caused the video download to fail part-way.",3:"The video playback was aborted due to a corruption problem or because the video used features your browser did not support.",4:"The video could not be loaded, either because the server or network failed or because the format is not supported.",5:"The video is encrypted and we do not have the keys to decrypt it."};for(var M=0;M<u.F.Ra.length;M++)u.F[u.F.Ra[M]]=M,u.F.prototype[u.F.Ra[M]]=M;
var N,O,P,Q;
N=["requestFullscreen exitFullscreen fullscreenElement fullscreenEnabled fullscreenchange fullscreenerror".split(" "),"webkitRequestFullscreen webkitExitFullscreen webkitFullscreenElement webkitFullscreenEnabled webkitfullscreenchange webkitfullscreenerror".split(" "),"webkitRequestFullScreen webkitCancelFullScreen webkitCurrentFullScreenElement webkitCancelFullScreen webkitfullscreenchange webkitfullscreenerror".split(" "),"mozRequestFullScreen mozCancelFullScreen mozFullScreenElement mozFullScreenEnabled mozfullscreenchange mozfullscreenerror".split(" "),"msRequestFullscreen msExitFullscreen msFullscreenElement msFullscreenEnabled MSFullscreenChange MSFullscreenError".split(" ")];
O=N[0];for(Q=0;Q<N.length;Q++)if(N[Q][1]in document){P=N[Q];break}if(P){u.Na.Bb={};for(Q=0;Q<P.length;Q++)u.Na.Bb[O[Q]]=P[Q]}
u.Player=u.a.extend({h:function(a,c,d){this.P=a;a.id=a.id||"vjs_video_"+u.v++;c=u.l.B(da(a),c);this.z={};this.Cc=c.poster;this.yb=c.controls;a.controls=l;c.Fc=l;u.a.call(this,this,c,d);this.controls()?this.o("vjs-controls-enabled"):this.o("vjs-controls-disabled");u.Aa[this.T]=this;c.plugins&&u.l.wa(c.plugins,function(a,c){this[a](c)},this);var e,g,h,k,p,n;e=u.bind(this,this.reportUserActivity);this.d("mousedown",function(){e();clearInterval(g);g=setInterval(e,250)});this.d("mousemove",function(a){if(a.screenX!=
p||a.screenY!=n)p=a.screenX,n=a.screenY,e()});this.d("mouseup",function(){e();clearInterval(g)});this.d("keydown",e);this.d("keyup",e);h=setInterval(u.bind(this,function(){this.na&&(this.na=l,this.userActive(f),clearTimeout(k),k=setTimeout(u.bind(this,function(){this.na||this.userActive(l)}),2E3))}),250);this.d("dispose",function(){clearInterval(h);clearTimeout(k)})}});t=u.Player.prototype;t.j=u.options;
t.dispose=function(){this.k("dispose");this.p("dispose");u.Aa[this.T]=j;this.P&&this.P.player&&(this.P.player=j);this.b&&this.b.player&&(this.b.player=j);clearInterval(this.Ya);this.Ba();this.g&&this.g.dispose();u.a.prototype.dispose.call(this)};function da(a){var c={sources:[],tracks:[]};u.l.B(c,u.Cb(a));if(a.hasChildNodes()){var d,e,g,h;a=a.childNodes;g=0;for(h=a.length;g<h;g++)d=a[g],e=d.nodeName.toLowerCase(),"source"===e?c.sources.push(u.Cb(d)):"track"===e&&c.tracks.push(u.Cb(d))}return c}
t.e=function(){var a=this.b=u.a.prototype.e.call(this,"div"),c=this.P;c.removeAttribute("width");c.removeAttribute("height");if(c.hasChildNodes()){var d,e,g,h,k;d=c.childNodes;e=d.length;for(k=[];e--;)g=d[e],h=g.nodeName.toLowerCase(),"track"===h&&k.push(g);for(d=0;d<k.length;d++)c.removeChild(k[d])}a.id=c.id;a.className=c.className;c.id+="_html5_api";c.className="vjs-tech";c.player=a.player=this;this.o("vjs-paused");this.width(this.j.width,f);this.height(this.j.height,f);c.parentNode&&c.parentNode.insertBefore(a,
c);u.Eb(c,a);this.b=a;this.d("loadstart",this.Cd);this.d("ended",this.yd);this.d("play",this.Ob);this.d("firstplay",this.Ad);this.d("pause",this.Nb);this.d("progress",this.Dd);this.d("durationchange",this.zc);this.d("fullscreenchange",this.Bd);return a};
function R(a,c,d){a.g&&(a.ca=l,a.g.dispose(),a.Ib&&(a.Ib=l,clearInterval(a.Ya)),a.Jb&&S(a),a.g=l);"Html5"!==c&&a.P&&(u.f.nc(a.P),a.P=j);a.Ca=c;a.ca=l;var e=u.l.B({source:d,parentEl:a.b},a.j[c.toLowerCase()]);d&&(d.src==a.z.src&&0<a.z.currentTime&&(e.startTime=a.z.currentTime),a.z.src=d.src);a.g=new window.videojs[c](a,e);a.g.K(function(){this.c.Ea();if(!this.n.progressEvents){var a=this.c;a.Ib=f;a.Ya=setInterval(u.bind(a,function(){this.z.tb<this.buffered().end(0)?this.k("progress"):1==this.bufferedPercent()&&
(clearInterval(this.Ya),this.k("progress"))}),500);a.g&&a.g.W("progress",function(){this.n.progressEvents=f;var a=this.c;a.Ib=l;clearInterval(a.Ya)})}this.n.timeupdateEvents||(a=this.c,a.Jb=f,a.d("play",a.Lc),a.d("pause",a.Ba),a.g&&a.g.W("timeupdate",function(){this.n.timeupdateEvents=f;S(this.c)}))})}function S(a){a.Jb=l;a.Ba();a.p("play",a.Lc);a.p("pause",a.Ba)}t.Lc=function(){this.mc&&this.Ba();this.mc=setInterval(u.bind(this,function(){this.k("timeupdate")}),250)};
t.Ba=function(){clearInterval(this.mc);this.k("timeupdate")};t.Cd=function(){this.error(j);this.paused()?(T(this,l),this.W("play",function(){T(this,f)})):this.k("firstplay")};t.uc=l;function T(a,c){c!==b&&a.uc!==c&&((a.uc=c)?(a.o("vjs-has-started"),a.k("firstplay")):a.r("vjs-has-started"))}t.Ob=function(){u.r(this.b,"vjs-paused");u.o(this.b,"vjs-playing")};t.Ad=function(){this.j.starttime&&this.currentTime(this.j.starttime);this.o("vjs-has-started")};
t.Nb=function(){u.r(this.b,"vjs-playing");u.o(this.b,"vjs-paused")};t.Dd=function(){1==this.bufferedPercent()&&this.k("loadedalldata")};t.yd=function(){this.j.loop&&(this.currentTime(0),this.play())};t.zc=function(){var a=U(this,"duration");a&&(0>a&&(a=Infinity),this.duration(a),Infinity===a?this.o("vjs-live"):this.r("vjs-live"))};t.Bd=function(){this.isFullscreen()?this.o("vjs-fullscreen"):this.r("vjs-fullscreen")};
function V(a,c,d){if(a.g&&!a.g.ca)a.g.K(function(){this[c](d)});else try{a.g[c](d)}catch(e){throw u.log(e),e;}}function U(a,c){if(a.g&&a.g.ca)try{return a.g[c]()}catch(d){throw a.g[c]===b?u.log("Video.js: "+c+" method not defined for "+a.Ca+" playback technology.",d):"TypeError"==d.name?(u.log("Video.js: "+c+" unavailable on "+a.Ca+" playback technology element.",d),a.g.ca=l):u.log(d),d;}}t.play=function(){V(this,"play");return this};t.pause=function(){V(this,"pause");return this};
t.paused=function(){return U(this,"paused")===l?l:f};t.currentTime=function(a){return a!==b?(V(this,"setCurrentTime",a),this.Jb&&this.k("timeupdate"),this):this.z.currentTime=U(this,"currentTime")||0};t.duration=function(a){if(a!==b)return this.z.duration=parseFloat(a),this;this.z.duration===b&&this.zc();return this.z.duration||0};t.buffered=function(){var a=U(this,"buffered"),c=a.length-1,d=this.z.tb=this.z.tb||0;a&&(0<=c&&a.end(c)!==d)&&(d=a.end(c),this.z.tb=d);return u.zb(0,d)};
t.bufferedPercent=function(){return this.duration()?this.buffered().end(0)/this.duration():0};t.volume=function(a){if(a!==b)return a=Math.max(0,Math.min(1,parseFloat(a))),this.z.volume=a,V(this,"setVolume",a),u.Ld(a),this;a=parseFloat(U(this,"volume"));return isNaN(a)?1:a};t.muted=function(a){return a!==b?(V(this,"setMuted",a),this):U(this,"muted")||l};t.ab=function(){return U(this,"supportsFullScreen")||l};t.wc=l;t.isFullscreen=function(a){return a!==b?(this.wc=!!a,this):this.wc};
t.isFullScreen=function(a){u.log.warn('player.isFullScreen() has been deprecated, use player.isFullscreen() with a lowercase "s")');return this.isFullscreen(a)};
t.requestFullscreen=function(){var a=u.Na.Bb;this.isFullscreen(f);a?(u.d(document,a.fullscreenchange,u.bind(this,function(c){this.isFullscreen(document[a.fullscreenElement]);this.isFullscreen()===l&&u.p(document,a.fullscreenchange,arguments.callee);this.k("fullscreenchange")})),this.b[a.requestFullscreen]()):this.g.ab()?V(this,"enterFullScreen"):(this.td=f,this.ld=document.documentElement.style.overflow,u.d(document,"keydown",u.bind(this,this.qc)),document.documentElement.style.overflow="hidden",
u.o(document.body,"vjs-full-window"),this.k("enterFullWindow"),this.k("fullscreenchange"));return this};t.exitFullscreen=function(){var a=u.Na.Bb;this.isFullscreen(l);if(a)document[a.exitFullscreen]();else this.g.ab()?V(this,"exitFullScreen"):(ea(this),this.k("fullscreenchange"));return this};t.qc=function(a){27===a.keyCode&&(this.isFullscreen()===f?this.exitFullscreen():ea(this))};
function ea(a){a.td=l;u.p(document,"keydown",a.qc);document.documentElement.style.overflow=a.ld;u.r(document.body,"vjs-full-window");a.k("exitFullWindow")}
t.src=function(a){if(a===b)return U(this,"src");if(a instanceof Array){var c;a:{c=a;for(var d=0,e=this.j.techOrder;d<e.length;d++){var g=u.$(e[d]),h=window.videojs[g];if(h){if(h.isSupported())for(var k=0,p=c;k<p.length;k++){var n=p[k];if(h.canPlaySource(n)){c={source:n,g:g};break a}}}else u.log.error('The "'+g+'" tech is undefined. Skipped browser support check for that tech.')}c=l}c?(a=c.source,c=c.g,c==this.Ca?this.src(a):R(this,c,a)):(this.error({code:4,message:this.options().notSupportedMessage}),
this.Ea())}else a instanceof Object?window.videojs[this.Ca].canPlaySource(a)?this.src(a.src):this.src([a]):(this.z.src=a,this.ca?(V(this,"src",a),"auto"==this.j.preload&&this.load(),this.j.autoplay&&this.play()):this.K(function(){this.src(a)}));return this};t.load=function(){V(this,"load");return this};t.currentSrc=function(){return U(this,"currentSrc")||this.z.src||""};t.Xa=function(a){return a!==b?(V(this,"setPreload",a),this.j.preload=a,this):U(this,"preload")};
t.autoplay=function(a){return a!==b?(V(this,"setAutoplay",a),this.j.autoplay=a,this):U(this,"autoplay")};t.loop=function(a){return a!==b?(V(this,"setLoop",a),this.j.loop=a,this):U(this,"loop")};t.poster=function(a){if(a===b)return this.Cc;this.Cc=a;V(this,"setPoster",a);this.k("posterchange")};
t.controls=function(a){return a!==b?(a=!!a,this.yb!==a&&((this.yb=a)?(this.r("vjs-controls-disabled"),this.o("vjs-controls-enabled"),this.k("controlsenabled")):(this.r("vjs-controls-enabled"),this.o("vjs-controls-disabled"),this.k("controlsdisabled"))),this):this.yb};u.Player.prototype.Tb;t=u.Player.prototype;
t.usingNativeControls=function(a){return a!==b?(a=!!a,this.Tb!==a&&((this.Tb=a)?(this.o("vjs-using-native-controls"),this.k("usingnativecontrols")):(this.r("vjs-using-native-controls"),this.k("usingcustomcontrols"))),this):this.Tb};t.ba=j;t.error=function(a){if(a===b)return this.ba;if(a===j)return this.ba=a,this.r("vjs-error"),this;this.ba=a instanceof u.F?a:new u.F(a);this.k("error");this.o("vjs-error");u.log.error("(CODE:"+this.ba.code+" "+u.F.Ra[this.ba.code]+")",this.ba.message,this.ba);return this};
t.ended=function(){return U(this,"ended")};t.seeking=function(){return U(this,"seeking")};t.na=f;t.reportUserActivity=function(){this.na=f};t.Sb=f;t.userActive=function(a){return a!==b?(a=!!a,a!==this.Sb&&((this.Sb=a)?(this.na=f,this.r("vjs-user-inactive"),this.o("vjs-user-active"),this.k("useractive")):(this.na=l,this.g&&this.g.W("mousemove",function(a){a.stopPropagation();a.preventDefault()}),this.r("vjs-user-active"),this.o("vjs-user-inactive"),this.k("userinactive"))),this):this.Sb};
t.playbackRate=function(a){return a!==b?(V(this,"setPlaybackRate",a),this):this.g&&this.g.n&&this.g.n.playbackRate?U(this,"playbackRate"):1};u.Ha=u.a.extend();u.Ha.prototype.j={fe:"play",children:{playToggle:{},currentTimeDisplay:{},timeDivider:{},durationDisplay:{},remainingTimeDisplay:{},liveDisplay:{},progressControl:{},fullscreenToggle:{},volumeControl:{},muteToggle:{},playbackRateMenuButton:{}}};u.Ha.prototype.e=function(){return u.e("div",{className:"vjs-control-bar"})};
u.Zb=u.a.extend({h:function(a,c){u.a.call(this,a,c)}});u.Zb.prototype.e=function(){var a=u.a.prototype.e.call(this,"div",{className:"vjs-live-controls vjs-control"});this.u=u.e("div",{className:"vjs-live-display",innerHTML:'<span class="vjs-control-text">Stream Type </span>LIVE',"aria-live":"off"});a.appendChild(this.u);return a};u.bc=u.s.extend({h:function(a,c){u.s.call(this,a,c);a.d("play",u.bind(this,this.Ob));a.d("pause",u.bind(this,this.Nb))}});t=u.bc.prototype;t.sa="Play";
t.S=function(){return"vjs-play-control "+u.s.prototype.S.call(this)};t.q=function(){this.c.paused()?this.c.play():this.c.pause()};t.Ob=function(){u.r(this.b,"vjs-paused");u.o(this.b,"vjs-playing");this.b.children[0].children[0].innerHTML="Pause"};t.Nb=function(){u.r(this.b,"vjs-playing");u.o(this.b,"vjs-paused");this.b.children[0].children[0].innerHTML="Play"};u.fb=u.a.extend({h:function(a,c){u.a.call(this,a,c);a.d("timeupdate",u.bind(this,this.fa))}});
u.fb.prototype.e=function(){var a=u.a.prototype.e.call(this,"div",{className:"vjs-current-time vjs-time-controls vjs-control"});this.u=u.e("div",{className:"vjs-current-time-display",innerHTML:'<span class="vjs-control-text">Current Time </span>0:00',"aria-live":"off"});a.appendChild(this.u);return a};u.fb.prototype.fa=function(){var a=this.c.$a?this.c.z.currentTime:this.c.currentTime();this.u.innerHTML='<span class="vjs-control-text">Current Time </span>'+u.ya(a,this.c.duration())};
u.gb=u.a.extend({h:function(a,c){u.a.call(this,a,c);a.d("timeupdate",u.bind(this,this.fa))}});u.gb.prototype.e=function(){var a=u.a.prototype.e.call(this,"div",{className:"vjs-duration vjs-time-controls vjs-control"});this.u=u.e("div",{className:"vjs-duration-display",innerHTML:'<span class="vjs-control-text">Duration Time </span>0:00',"aria-live":"off"});a.appendChild(this.u);return a};
u.gb.prototype.fa=function(){var a=this.c.duration();a&&(this.u.innerHTML='<span class="vjs-control-text">Duration Time </span>'+u.ya(a))};u.hc=u.a.extend({h:function(a,c){u.a.call(this,a,c)}});u.hc.prototype.e=function(){return u.a.prototype.e.call(this,"div",{className:"vjs-time-divider",innerHTML:"<div><span>/</span></div>"})};u.nb=u.a.extend({h:function(a,c){u.a.call(this,a,c);a.d("timeupdate",u.bind(this,this.fa))}});
u.nb.prototype.e=function(){var a=u.a.prototype.e.call(this,"div",{className:"vjs-remaining-time vjs-time-controls vjs-control"});this.u=u.e("div",{className:"vjs-remaining-time-display",innerHTML:'<span class="vjs-control-text">Remaining Time </span>-0:00',"aria-live":"off"});a.appendChild(this.u);return a};u.nb.prototype.fa=function(){this.c.duration()&&(this.u.innerHTML='<span class="vjs-control-text">Remaining Time </span>-'+u.ya(this.c.duration()-this.c.currentTime()))};
u.Ia=u.s.extend({h:function(a,c){u.s.call(this,a,c)}});u.Ia.prototype.sa="Fullscreen";u.Ia.prototype.S=function(){return"vjs-fullscreen-control "+u.s.prototype.S.call(this)};u.Ia.prototype.q=function(){this.c.isFullscreen()?(this.c.exitFullscreen(),this.xb.innerHTML="Fullscreen"):(this.c.requestFullscreen(),this.xb.innerHTML="Non-Fullscreen")};u.mb=u.a.extend({h:function(a,c){u.a.call(this,a,c)}});u.mb.prototype.j={children:{seekBar:{}}};
u.mb.prototype.e=function(){return u.a.prototype.e.call(this,"div",{className:"vjs-progress-control vjs-control"})};u.dc=u.Q.extend({h:function(a,c){u.Q.call(this,a,c);a.d("timeupdate",u.bind(this,this.ma));a.K(u.bind(this,this.ma))}});t=u.dc.prototype;t.j={children:{loadProgressBar:{},playProgressBar:{},seekHandle:{}},barName:"playProgressBar",handleName:"seekHandle"};t.Bc="timeupdate";t.e=function(){return u.Q.prototype.e.call(this,"div",{className:"vjs-progress-holder","aria-label":"video progress bar"})};
t.ma=function(){var a=this.c.$a?this.c.z.currentTime:this.c.currentTime();this.b.setAttribute("aria-valuenow",u.round(100*this.Db(),2));this.b.setAttribute("aria-valuetext",u.ya(a,this.c.duration()))};t.Db=function(){return this.c.currentTime()/this.c.duration()};t.Wa=function(a){u.Q.prototype.Wa.call(this,a);this.c.$a=f;this.Xd=!this.c.paused();this.c.pause()};t.Lb=function(a){a=J(this,a)*this.c.duration();a==this.c.duration()&&(a-=0.1);this.c.currentTime(a)};
t.Mb=function(a){u.Q.prototype.Mb.call(this,a);this.c.$a=l;this.Xd&&this.c.play()};t.Ic=function(){this.c.currentTime(this.c.currentTime()+5)};t.Hc=function(){this.c.currentTime(this.c.currentTime()-5)};u.jb=u.a.extend({h:function(a,c){u.a.call(this,a,c);a.d("progress",u.bind(this,this.update))}});u.jb.prototype.e=function(){return u.a.prototype.e.call(this,"div",{className:"vjs-load-progress",innerHTML:'<span class="vjs-control-text">Loaded: 0%</span>'})};
u.jb.prototype.update=function(){this.b.style&&(this.b.style.width=u.round(100*this.c.bufferedPercent(),2)+"%")};u.ac=u.a.extend({h:function(a,c){u.a.call(this,a,c)}});u.ac.prototype.e=function(){return u.a.prototype.e.call(this,"div",{className:"vjs-play-progress",innerHTML:'<span class="vjs-control-text">Progress: 0%</span>'})};u.Ka=u.Y.extend({h:function(a,c){u.Y.call(this,a,c);a.d("timeupdate",u.bind(this,this.fa))}});u.Ka.prototype.defaultValue="00:00";
u.Ka.prototype.e=function(){return u.Y.prototype.e.call(this,"div",{className:"vjs-seek-handle","aria-live":"off"})};u.Ka.prototype.fa=function(){var a=this.c.$a?this.c.z.currentTime:this.c.currentTime();this.b.innerHTML='<span class="vjs-control-text">'+u.ya(a,this.c.duration())+"</span>"};u.pb=u.a.extend({h:function(a,c){u.a.call(this,a,c);a.g&&(a.g.n&&a.g.n.volumeControl===l)&&this.o("vjs-hidden");a.d("loadstart",u.bind(this,function(){a.g.n&&a.g.n.volumeControl===l?this.o("vjs-hidden"):this.r("vjs-hidden")}))}});
u.pb.prototype.j={children:{volumeBar:{}}};u.pb.prototype.e=function(){return u.a.prototype.e.call(this,"div",{className:"vjs-volume-control vjs-control"})};u.ob=u.Q.extend({h:function(a,c){u.Q.call(this,a,c);a.d("volumechange",u.bind(this,this.ma));a.K(u.bind(this,this.ma))}});t=u.ob.prototype;t.ma=function(){this.b.setAttribute("aria-valuenow",u.round(100*this.c.volume(),2));this.b.setAttribute("aria-valuetext",u.round(100*this.c.volume(),2)+"%")};
t.j={children:{volumeLevel:{},volumeHandle:{}},barName:"volumeLevel",handleName:"volumeHandle"};t.Bc="volumechange";t.e=function(){return u.Q.prototype.e.call(this,"div",{className:"vjs-volume-bar","aria-label":"volume level"})};t.Lb=function(a){this.c.muted()&&this.c.muted(l);this.c.volume(J(this,a))};t.Db=function(){return this.c.muted()?0:this.c.volume()};t.Ic=function(){this.c.volume(this.c.volume()+0.1)};t.Hc=function(){this.c.volume(this.c.volume()-0.1)};
u.ic=u.a.extend({h:function(a,c){u.a.call(this,a,c)}});u.ic.prototype.e=function(){return u.a.prototype.e.call(this,"div",{className:"vjs-volume-level",innerHTML:'<span class="vjs-control-text"></span>'})};u.qb=u.Y.extend();u.qb.prototype.defaultValue="00:00";u.qb.prototype.e=function(){return u.Y.prototype.e.call(this,"div",{className:"vjs-volume-handle"})};
u.ha=u.s.extend({h:function(a,c){u.s.call(this,a,c);a.d("volumechange",u.bind(this,this.update));a.g&&(a.g.n&&a.g.n.volumeControl===l)&&this.o("vjs-hidden");a.d("loadstart",u.bind(this,function(){a.g.n&&a.g.n.volumeControl===l?this.o("vjs-hidden"):this.r("vjs-hidden")}))}});u.ha.prototype.e=function(){return u.s.prototype.e.call(this,"div",{className:"vjs-mute-control vjs-control",innerHTML:'<div><span class="vjs-control-text">Mute</span></div>'})};
u.ha.prototype.q=function(){this.c.muted(this.c.muted()?l:f)};u.ha.prototype.update=function(){var a=this.c.volume(),c=3;0===a||this.c.muted()?c=0:0.33>a?c=1:0.67>a&&(c=2);this.c.muted()?"Unmute"!=this.b.children[0].children[0].innerHTML&&(this.b.children[0].children[0].innerHTML="Unmute"):"Mute"!=this.b.children[0].children[0].innerHTML&&(this.b.children[0].children[0].innerHTML="Mute");for(a=0;4>a;a++)u.r(this.b,"vjs-vol-"+a);u.o(this.b,"vjs-vol-"+c)};
u.qa=u.L.extend({h:function(a,c){u.L.call(this,a,c);a.d("volumechange",u.bind(this,this.update));a.g&&(a.g.n&&a.g.n.Oc===l)&&this.o("vjs-hidden");a.d("loadstart",u.bind(this,function(){a.g.n&&a.g.n.Oc===l?this.o("vjs-hidden"):this.r("vjs-hidden")}));this.o("vjs-menu-button")}});u.qa.prototype.va=function(){var a=new u.ga(this.c,{lc:"div"}),c=new u.ob(this.c,u.l.B({Wd:f},this.j.me));a.V(c);return a};u.qa.prototype.q=function(){u.ha.prototype.q.call(this);u.L.prototype.q.call(this)};
u.qa.prototype.e=function(){return u.s.prototype.e.call(this,"div",{className:"vjs-volume-menu-button vjs-menu-button vjs-control",innerHTML:'<div><span class="vjs-control-text">Mute</span></div>'})};u.qa.prototype.update=u.ha.prototype.update;u.cc=u.L.extend({h:function(a,c){u.L.call(this,a,c);this.Nc();this.Mc();a.d("loadstart",u.bind(this,this.Nc));a.d("ratechange",u.bind(this,this.Mc))}});t=u.cc.prototype;
t.e=function(){var a=u.a.prototype.e.call(this,"div",{className:"vjs-playback-rate vjs-menu-button vjs-control",innerHTML:'<div class="vjs-control-content"><span class="vjs-control-text">Playback Rate</span></div>'});this.yc=u.e("div",{className:"vjs-playback-rate-value",innerHTML:1});a.appendChild(this.yc);return a};t.va=function(){var a=new u.ga(this.m()),c=this.m().options().playbackRates;if(c)for(var d=c.length-1;0<=d;d--)a.V(new u.lb(this.m(),{rate:c[d]+"x"}));return a};
t.ma=function(){this.w().setAttribute("aria-valuenow",this.m().playbackRate())};t.q=function(){for(var a=this.m().playbackRate(),c=this.m().options().playbackRates,d=c[0],e=0;e<c.length;e++)if(c[e]>a){d=c[e];break}this.m().playbackRate(d)};function fa(a){return a.m().g&&a.m().g.n.playbackRate&&a.m().options().playbackRates&&0<a.m().options().playbackRates.length}t.Nc=function(){fa(this)?this.r("vjs-hidden"):this.o("vjs-hidden")};
t.Mc=function(){fa(this)&&(this.yc.innerHTML=this.m().playbackRate()+"x")};u.lb=u.I.extend({lc:"button",h:function(a,c){var d=this.label=c.rate,e=this.Dc=parseFloat(d,10);c.label=d;c.selected=1===e;u.I.call(this,a,c);this.m().d("ratechange",u.bind(this,this.update))}});u.lb.prototype.q=function(){u.I.prototype.q.call(this);this.m().playbackRate(this.Dc)};u.lb.prototype.update=function(){this.selected(this.m().playbackRate()==this.Dc)};
u.Ja=u.s.extend({h:function(a,c){u.s.call(this,a,c);a.poster()&&this.src(a.poster());(!a.poster()||!a.controls())&&this.G();a.d("posterchange",u.bind(this,function(){this.src(a.poster())}));a.d("play",u.bind(this,this.G))}});var ga="backgroundSize"in u.A.style;u.Ja.prototype.e=function(){var a=u.e("div",{className:"vjs-poster",tabIndex:-1});ga||a.appendChild(u.e("img"));return a};u.Ja.prototype.src=function(a){var c=this.w();a!==b&&(ga?c.style.backgroundImage='url("'+a+'")':c.firstChild.src=a)};
u.Ja.prototype.q=function(){this.m().controls()&&this.c.play()};u.$b=u.a.extend({h:function(a,c){u.a.call(this,a,c);a.d("canplay",u.bind(this,this.G));a.d("canplaythrough",u.bind(this,this.G));a.d("playing",u.bind(this,this.G));a.d("seeking",u.bind(this,this.show));a.d("seeked",u.bind(this,this.G));a.d("ended",u.bind(this,this.G));a.d("waiting",u.bind(this,this.show))}});u.$b.prototype.e=function(){return u.a.prototype.e.call(this,"div",{className:"vjs-loading-spinner"})};u.cb=u.s.extend();
u.cb.prototype.e=function(){return u.s.prototype.e.call(this,"div",{className:"vjs-big-play-button",innerHTML:'<span aria-hidden="true"></span>',"aria-label":"play video"})};u.cb.prototype.q=function(){this.c.play()};u.hb=u.a.extend({h:function(a,c){u.a.call(this,a,c);this.update();a.d("error",u.bind(this,this.update))}});u.hb.prototype.e=function(){var a=u.a.prototype.e.call(this,"div",{className:"vjs-error-display"});this.u=u.e("div");a.appendChild(this.u);return a};
u.hb.prototype.update=function(){this.m().error()&&(this.u.innerHTML=this.m().error().message)};
u.t=u.a.extend({h:function(a,c,d){c=c||{};c.Fc=l;u.a.call(this,a,c,d);var e,g;g=this;e=this.m();a=function(){if(e.controls()&&!e.usingNativeControls()){var a;g.d("mousedown",g.q);g.d("touchstart",function(c){c.preventDefault();a=this.c.userActive()});g.d("touchmove",function(){a&&this.m().reportUserActivity()});I(g);g.d("tap",g.Ed)}};c=u.bind(g,g.Id);this.K(a);e.d("controlsenabled",a);e.d("controlsdisabled",c)}});t=u.t.prototype;
t.Id=function(){this.p("tap");this.p("touchstart");this.p("touchmove");this.p("touchleave");this.p("touchcancel");this.p("touchend");this.p("click");this.p("mousedown")};t.q=function(a){0===a.button&&this.m().controls()&&(this.m().paused()?this.m().play():this.m().pause())};t.Ed=function(){this.m().userActive(!this.m().userActive())};t.Qb=m();t.n={volumeControl:f,fullscreenResize:l,playbackRate:l,progressEvents:l,timeupdateEvents:l};u.media={};u.media.bb="play pause paused currentTime setCurrentTime duration buffered volume setVolume muted setMuted width height supportsFullScreen enterFullScreen src load currentSrc preload setPreload autoplay setAutoplay loop setLoop error networkState readyState seeking initialTime startOffsetTime played seekable ended videoTracks audioTracks videoWidth videoHeight textTracks defaultPlaybackRate playbackRate mediaGroup controller controls defaultMuted".split(" ");
function ha(){var a=u.media.bb[i];return function(){throw Error('The "'+a+"\" method is not available on the playback technology's API");}}for(var i=u.media.bb.length-1;0<=i;i--)u.t.prototype[u.media.bb[i]]=ha();
u.f=u.t.extend({h:function(a,c,d){this.n.volumeControl=u.f.ed();this.n.playbackRate=u.f.dd();this.n.movingMediaElementInDOM=!u.Tc;this.n.fullscreenResize=f;u.t.call(this,a,c,d);for(d=u.f.ib.length-1;0<=d;d--)u.d(this.b,u.f.ib[d],u.bind(this,this.nd));(c=c.source)&&this.b.currentSrc===c.src&&0<this.b.networkState?a.k("loadstart"):c&&(this.b.src=c.src);if(u.fc&&a.options().nativeControlsForTouch!==l){var e,g,h,k;e=this;g=this.m();c=g.controls();e.b.controls=!!c;h=function(){e.b.controls=f};k=function(){e.b.controls=
l};g.d("controlsenabled",h);g.d("controlsdisabled",k);c=function(){g.p("controlsenabled",h);g.p("controlsdisabled",k)};e.d("dispose",c);g.d("usingcustomcontrols",c);g.usingNativeControls(f)}a.K(function(){this.P&&(this.j.autoplay&&this.paused())&&(delete this.P.poster,this.play())});this.Ea()}});t=u.f.prototype;t.dispose=function(){u.t.prototype.dispose.call(this)};
t.e=function(){var a=this.c,c=a.P,d;if(!c||this.n.movingMediaElementInDOM===l)c?(d=c.cloneNode(l),u.f.nc(c),c=d,a.P=j):c=u.e("video",{id:a.id()+"_html5_api",className:"vjs-tech"}),c.player=a,u.Eb(c,a.w());d=["autoplay","preload","loop","muted"];for(var e=d.length-1;0<=e;e--){var g=d[e];a.j[g]!==j&&(c[g]=a.j[g])}return c};t.nd=function(a){"error"==a.type?this.m().error(this.error().code):(a.bubbles=l,this.m().k(a))};t.play=function(){this.b.play()};t.pause=function(){this.b.pause()};t.paused=function(){return this.b.paused};
t.currentTime=function(){return this.b.currentTime};t.Kd=function(a){try{this.b.currentTime=a}catch(c){u.log(c,"Video is not ready. (Video.js)")}};t.duration=function(){return this.b.duration||0};t.buffered=function(){return this.b.buffered};t.volume=function(){return this.b.volume};t.Qd=function(a){this.b.volume=a};t.muted=function(){return this.b.muted};t.Nd=function(a){this.b.muted=a};t.width=function(){return this.b.offsetWidth};t.height=function(){return this.b.offsetHeight};
t.ab=function(){return"function"==typeof this.b.webkitEnterFullScreen&&(/Android/.test(u.M)||!/Chrome|Mac OS X 10.5/.test(u.M))?f:l};t.oc=function(){var a=this.b;a.paused&&a.networkState<=a.Zd?(this.b.play(),setTimeout(function(){a.pause();a.webkitEnterFullScreen()},0)):a.webkitEnterFullScreen()};t.od=function(){this.b.webkitExitFullScreen()};t.src=function(a){this.b.src=a};t.load=function(){this.b.load()};t.currentSrc=function(){return this.b.currentSrc};t.poster=function(){return this.b.poster};
t.Qb=function(a){this.b.poster=a};t.Xa=function(){return this.b.Xa};t.Pd=function(a){this.b.Xa=a};t.autoplay=function(){return this.b.autoplay};t.Jd=function(a){this.b.autoplay=a};t.controls=function(){return this.b.controls};t.loop=function(){return this.b.loop};t.Md=function(a){this.b.loop=a};t.error=function(){return this.b.error};t.seeking=function(){return this.b.seeking};t.ended=function(){return this.b.ended};t.playbackRate=function(){return this.b.playbackRate};
t.Od=function(a){this.b.playbackRate=a};u.f.isSupported=function(){try{u.A.volume=0.5}catch(a){return l}return!!u.A.canPlayType};u.f.ub=function(a){try{return!!u.A.canPlayType(a.type)}catch(c){return""}};u.f.ed=function(){var a=u.A.volume;u.A.volume=a/2+0.1;return a!==u.A.volume};u.f.dd=function(){var a=u.A.playbackRate;u.A.playbackRate=a/2+0.1;return a!==u.A.playbackRate};var W,ia=/^application\/(?:x-|vnd\.apple\.)mpegurl/i,ja=/^video\/mp4/i;
u.f.Ac=function(){4<=u.Ub&&(W||(W=u.A.constructor.prototype.canPlayType),u.A.constructor.prototype.canPlayType=function(a){return a&&ia.test(a)?"maybe":W.call(this,a)});u.Xc&&(W||(W=u.A.constructor.prototype.canPlayType),u.A.constructor.prototype.canPlayType=function(a){return a&&ja.test(a)?"maybe":W.call(this,a)})};u.f.Vd=function(){var a=u.A.constructor.prototype.canPlayType;u.A.constructor.prototype.canPlayType=W;W=j;return a};u.f.Ac();u.f.ib="loadstart suspend abort error emptied stalled loadedmetadata loadeddata canplay canplaythrough playing waiting seeking seeked ended durationchange timeupdate progress play pause ratechange volumechange".split(" ");
u.f.nc=function(a){if(a){a.player=j;for(a.parentNode&&a.parentNode.removeChild(a);a.hasChildNodes();)a.removeChild(a.firstChild);a.removeAttribute("src");if("function"===typeof a.load)try{a.load()}catch(c){}}};
u.i=u.t.extend({h:function(a,c,d){u.t.call(this,a,c,d);var e=c.source;d=c.parentEl;var g=this.b=u.e("div",{id:a.id()+"_temp_flash"}),h=a.id()+"_flash_api";a=a.j;var k=u.l.B({readyFunction:"videojs.Flash.onReady",eventProxyFunction:"videojs.Flash.onEvent",errorEventProxyFunction:"videojs.Flash.onError",autoplay:a.autoplay,preload:a.Xa,loop:a.loop,muted:a.muted},c.flashVars),p=u.l.B({wmode:"opaque",bgcolor:"#000000"},c.params),n=u.l.B({id:h,name:h,"class":"vjs-tech"},c.attributes),s;e&&(e.type&&u.i.vd(e.type)?
(a=u.i.Jc(e.src),k.rtmpConnection=encodeURIComponent(a.wb),k.rtmpStream=encodeURIComponent(a.Rb)):k.src=encodeURIComponent(u.rc(e.src)));this.setCurrentTime=function(a){s=a;this.b.vjs_setProperty("currentTime",a)};this.currentTime=function(){return this.seeking()?s:this.b.vjs_getProperty("currentTime")};u.Eb(g,d);c.startTime&&this.K(function(){this.load();this.play();this.currentTime(c.startTime)});u.Yb&&this.K(function(){u.d(this.w(),"mousemove",u.bind(this,function(){this.m().k({type:"mousemove",
bubbles:l})}))});if(c.iFrameMode===f&&!u.Yb){var C=u.e("iframe",{id:h+"_iframe",name:h+"_iframe",className:"vjs-tech",scrolling:"no",marginWidth:0,marginHeight:0,frameBorder:0});k.readyFunction="ready";k.eventProxyFunction="events";k.errorEventProxyFunction="errors";u.d(C,"load",u.bind(this,function(){var a,d=C.contentWindow;a=C.contentDocument?C.contentDocument:C.contentWindow.document;a.write(u.i.sc(c.swf,k,p,n));d.player=this.c;d.ready=u.bind(this.c,function(c){var d=this.g;d.b=a.getElementById(c);
u.i.vb(d)});d.events=u.bind(this.c,function(a,c){this&&"flash"===this.Ca&&this.k(c)});d.errors=u.bind(this.c,function(a,c){u.log("Flash Error",c)})}));g.parentNode.replaceChild(C,g)}else u.i.md(c.swf,g,k,p,n)}});t=u.i.prototype;t.dispose=function(){u.t.prototype.dispose.call(this)};t.play=function(){this.b.vjs_play()};t.pause=function(){this.b.vjs_pause()};
t.src=function(a){if(a===b)return this.currentSrc();u.i.ud(a)?(a=u.i.Jc(a),this.he(a.wb),this.ie(a.Rb)):(a=u.rc(a),this.b.vjs_src(a));if(this.c.autoplay()){var c=this;setTimeout(function(){c.play()},0)}};t.currentSrc=function(){var a=this.b.vjs_getProperty("currentSrc");if(a==j){var c=this.rtmpConnection(),d=this.rtmpStream();c&&d&&(a=u.i.Rd(c,d))}return a};t.load=function(){this.b.vjs_load()};t.poster=function(){this.b.vjs_getProperty("poster")};t.Qb=m();t.buffered=function(){return u.zb(0,this.b.vjs_getProperty("buffered"))};
t.ab=r(l);t.oc=r(l);var ka=u.i.prototype,X="rtmpConnection rtmpStream preload defaultPlaybackRate playbackRate autoplay loop mediaGroup controller controls volume muted defaultMuted".split(" "),la="error networkState readyState seeking initialTime duration startOffsetTime paused played seekable ended videoTracks audioTracks videoWidth videoHeight textTracks".split(" ");function ma(){var a=X[Y],c=a.charAt(0).toUpperCase()+a.slice(1);ka["set"+c]=function(c){return this.b.vjs_setProperty(a,c)}}
function na(a){ka[a]=function(){return this.b.vjs_getProperty(a)}}var Y;for(Y=0;Y<X.length;Y++)na(X[Y]),ma();for(Y=0;Y<la.length;Y++)na(la[Y]);u.i.isSupported=function(){return 10<=u.i.version()[0]};u.i.ub=function(a){if(!a.type)return"";a=a.type.replace(/;.*/,"").toLowerCase();if(a in u.i.qd||a in u.i.Kc)return"maybe"};u.i.qd={"video/flv":"FLV","video/x-flv":"FLV","video/mp4":"MP4","video/m4v":"MP4"};u.i.Kc={"rtmp/mp4":"MP4","rtmp/flv":"FLV"};
u.i.onReady=function(a){a=u.w(a);var c=a.player||a.parentNode.player,d=c.g;a.player=c;d.b=a;u.i.vb(d)};u.i.vb=function(a){a.w().vjs_getProperty?a.Ea():setTimeout(function(){u.i.vb(a)},50)};u.i.onEvent=function(a,c){u.w(a).player.k(c)};u.i.onError=function(a,c){var d=u.w(a).player,e="FLASH: "+c;"srcnotfound"==c?d.error({code:4,message:e}):d.error(e)};
u.i.version=function(){var a="0,0,0";try{a=(new window.ActiveXObject("ShockwaveFlash.ShockwaveFlash")).GetVariable("$version").replace(/\D+/g,",").match(/^,?(.+),?$/)[1]}catch(c){try{navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin&&(a=(navigator.plugins["Shockwave Flash 2.0"]||navigator.plugins["Shockwave Flash"]).description.replace(/\D+/g,",").match(/^,?(.+),?$/)[1])}catch(d){}}return a.split(",")};
u.i.md=function(a,c,d,e,g){a=u.i.sc(a,d,e,g);a=u.e("div",{innerHTML:a}).childNodes[0];d=c.parentNode;c.parentNode.replaceChild(a,c);var h=d.childNodes[0];setTimeout(function(){h.style.display="block"},1E3)};
u.i.sc=function(a,c,d,e){var g="",h="",k="";c&&u.l.wa(c,function(a,c){g+=a+"="+c+"&amp;"});d=u.l.B({movie:a,flashvars:g,allowScriptAccess:"always",allowNetworking:"all"},d);u.l.wa(d,function(a,c){h+='<param name="'+a+'" value="'+c+'" />'});e=u.l.B({data:a,width:"100%",height:"100%"},e);u.l.wa(e,function(a,c){k+=a+'="'+c+'" '});return'<object type="application/x-shockwave-flash"'+k+">"+h+"</object>"};u.i.Rd=function(a,c){return a+"&"+c};
u.i.Jc=function(a){var c={wb:"",Rb:""};if(!a)return c;var d=a.indexOf("&"),e;-1!==d?e=d+1:(d=e=a.lastIndexOf("/")+1,0===d&&(d=e=a.length));c.wb=a.substring(0,d);c.Rb=a.substring(e,a.length);return c};u.i.vd=function(a){return a in u.i.Kc};u.i.Zc=/^rtmp[set]?:\/\//i;u.i.ud=function(a){return u.i.Zc.test(a)};
u.Yc=u.a.extend({h:function(a,c,d){u.a.call(this,a,c,d);if(!a.j.sources||0===a.j.sources.length){c=0;for(d=a.j.techOrder;c<d.length;c++){var e=u.$(d[c]),g=window.videojs[e];if(g&&g.isSupported()){R(a,e);break}}}else a.src(a.j.sources)}});u.Player.prototype.textTracks=function(){return this.Da=this.Da||[]};
function oa(a,c,d,e,g){var h=a.Da=a.Da||[];g=g||{};g.kind=c;g.label=d;g.language=e;c=u.$(c||"subtitles");var k=new window.videojs[c+"Track"](a,g);h.push(k);k.Qa()&&a.K(function(){setTimeout(function(){k.show()},0)})}function pa(a,c,d){for(var e=a.Da,g=0,h=e.length,k,p;g<h;g++)k=e[g],k.id()===c?(k.show(),p=k):d&&(k.J()==d&&0<k.mode())&&k.disable();(c=p?p.J():d?d:l)&&a.k(c+"trackchange")}
u.C=u.a.extend({h:function(a,c){u.a.call(this,a,c);this.T=c.id||"vjs_"+c.kind+"_"+c.language+"_"+u.v++;this.Gc=c.src;this.jd=c["default"]||c.dflt;this.Td=c.title;this.ee=c.srclang;this.wd=c.label;this.aa=[];this.rb=[];this.ka=this.la=0;this.c.d("fullscreenchange",u.bind(this,this.ad))}});t=u.C.prototype;t.J=q("H");t.src=q("Gc");t.Qa=q("jd");t.title=q("Td");t.label=q("wd");t.fd=q("aa");t.$c=q("rb");t.readyState=q("la");t.mode=q("ka");
t.ad=function(){this.b.style.fontSize=this.c.isFullScreen()?140*(screen.width/this.c.width())+"%":""};t.e=function(){return u.a.prototype.e.call(this,"div",{className:"vjs-"+this.H+" vjs-text-track"})};t.show=function(){qa(this);this.ka=2;u.a.prototype.show.call(this)};t.G=function(){qa(this);this.ka=1;u.a.prototype.G.call(this)};
t.disable=function(){2==this.ka&&this.G();this.c.p("timeupdate",u.bind(this,this.update,this.T));this.c.p("ended",u.bind(this,this.reset,this.T));this.reset();this.c.ja("textTrackDisplay").removeChild(this);this.ka=0};function qa(a){0===a.la&&a.load();0===a.ka&&(a.c.d("timeupdate",u.bind(a,a.update,a.T)),a.c.d("ended",u.bind(a,a.reset,a.T)),("captions"===a.H||"subtitles"===a.H)&&a.c.ja("textTrackDisplay").V(a))}
t.load=function(){0===this.la&&(this.la=1,u.get(this.Gc,u.bind(this,this.Fd),u.bind(this,this.zd)))};t.zd=function(a){this.error=a;this.la=3;this.k("error")};t.Fd=function(a){var c,d;a=a.split("\n");for(var e="",g=1,h=a.length;g<h;g++)if(e=u.trim(a[g])){-1==e.indexOf("--\x3e")?(c=e,e=u.trim(a[++g])):c=this.aa.length;c={id:c,index:this.aa.length};d=e.split(" --\x3e ");c.startTime=ra(d[0]);c.xa=ra(d[1]);for(d=[];a[++g]&&(e=u.trim(a[g]));)d.push(e);c.text=d.join("<br/>");this.aa.push(c)}this.la=2;this.k("loaded")};
function ra(a){var c=a.split(":");a=0;var d,e,g;3==c.length?(d=c[0],e=c[1],c=c[2]):(d=0,e=c[0],c=c[1]);c=c.split(/\s+/);c=c.splice(0,1)[0];c=c.split(/\.|,/);g=parseFloat(c[1]);c=c[0];a+=3600*parseFloat(d);a+=60*parseFloat(e);a+=parseFloat(c);g&&(a+=g/1E3);return a}
t.update=function(){if(0<this.aa.length){var a=this.c.options().trackTimeOffset||0,a=this.c.currentTime()+a;if(this.Pb===b||a<this.Pb||this.Ta<=a){var c=this.aa,d=this.c.duration(),e=0,g=l,h=[],k,p,n,s;a>=this.Ta||this.Ta===b?s=this.Ab!==b?this.Ab:0:(g=f,s=this.Hb!==b?this.Hb:c.length-1);for(;;){n=c[s];if(n.xa<=a)e=Math.max(e,n.xa),n.Ma&&(n.Ma=l);else if(a<n.startTime){if(d=Math.min(d,n.startTime),n.Ma&&(n.Ma=l),!g)break}else g?(h.splice(0,0,n),p===b&&(p=s),k=s):(h.push(n),k===b&&(k=s),p=s),d=Math.min(d,
n.xa),e=Math.max(e,n.startTime),n.Ma=f;if(g)if(0===s)break;else s--;else if(s===c.length-1)break;else s++}this.rb=h;this.Ta=d;this.Pb=e;this.Ab=k;this.Hb=p;k=this.rb;p="";a=0;for(c=k.length;a<c;a++)p+='<span class="vjs-tt-cue">'+k[a].text+"</span>";this.b.innerHTML=p;this.k("cuechange")}}};t.reset=function(){this.Ta=0;this.Pb=this.c.duration();this.Hb=this.Ab=0};u.Wb=u.C.extend();u.Wb.prototype.H="captions";u.ec=u.C.extend();u.ec.prototype.H="subtitles";u.Xb=u.C.extend();u.Xb.prototype.H="chapters";
u.gc=u.a.extend({h:function(a,c,d){u.a.call(this,a,c,d);if(a.j.tracks&&0<a.j.tracks.length){c=this.c;a=a.j.tracks;for(var e=0;e<a.length;e++)d=a[e],oa(c,d.kind,d.label,d.language,d)}}});u.gc.prototype.e=function(){return u.a.prototype.e.call(this,"div",{className:"vjs-text-track-display"})};u.Z=u.I.extend({h:function(a,c){var d=this.ea=c.track;c.label=d.label();c.selected=d.Qa();u.I.call(this,a,c);this.c.d(d.J()+"trackchange",u.bind(this,this.update))}});
u.Z.prototype.q=function(){u.I.prototype.q.call(this);pa(this.c,this.ea.T,this.ea.J())};u.Z.prototype.update=function(){this.selected(2==this.ea.mode())};u.kb=u.Z.extend({h:function(a,c){c.track={J:function(){return c.kind},m:a,label:function(){return c.kind+" off"},Qa:r(l),mode:r(l)};u.Z.call(this,a,c);this.selected(f)}});u.kb.prototype.q=function(){u.Z.prototype.q.call(this);pa(this.c,this.ea.T,this.ea.J())};
u.kb.prototype.update=function(){for(var a=this.c.textTracks(),c=0,d=a.length,e,g=f;c<d;c++)e=a[c],e.J()==this.ea.J()&&2==e.mode()&&(g=l);this.selected(g)};u.U=u.L.extend({h:function(a,c){u.L.call(this,a,c);1>=this.O.length&&this.G()}});u.U.prototype.ua=function(){var a=[],c;a.push(new u.kb(this.c,{kind:this.H}));for(var d=0;d<this.c.textTracks().length;d++)c=this.c.textTracks()[d],c.J()===this.H&&a.push(new u.Z(this.c,{track:c}));return a};
u.Fa=u.U.extend({h:function(a,c,d){u.U.call(this,a,c,d);this.b.setAttribute("aria-label","Captions Menu")}});u.Fa.prototype.H="captions";u.Fa.prototype.sa="Captions";u.Fa.prototype.className="vjs-captions-button";u.La=u.U.extend({h:function(a,c,d){u.U.call(this,a,c,d);this.b.setAttribute("aria-label","Subtitles Menu")}});u.La.prototype.H="subtitles";u.La.prototype.sa="Subtitles";u.La.prototype.className="vjs-subtitles-button";
u.Ga=u.U.extend({h:function(a,c,d){u.U.call(this,a,c,d);this.b.setAttribute("aria-label","Chapters Menu")}});t=u.Ga.prototype;t.H="chapters";t.sa="Chapters";t.className="vjs-chapters-button";t.ua=function(){for(var a=[],c,d=0;d<this.c.textTracks().length;d++)c=this.c.textTracks()[d],c.J()===this.H&&a.push(new u.Z(this.c,{track:c}));return a};
t.va=function(){for(var a=this.c.textTracks(),c=0,d=a.length,e,g,h=this.O=[];c<d;c++)if(e=a[c],e.J()==this.H&&e.Qa()){if(2>e.readyState()){this.be=e;e.d("loaded",u.bind(this,this.va));return}g=e;break}a=this.za=new u.ga(this.c);a.ia().appendChild(u.e("li",{className:"vjs-menu-title",innerHTML:u.$(this.H),Sd:-1}));if(g){e=g.aa;for(var k,c=0,d=e.length;c<d;c++)k=e[c],k=new u.eb(this.c,{track:g,cue:k}),h.push(k),a.V(k)}0<this.O.length&&this.show();return a};
u.eb=u.I.extend({h:function(a,c){var d=this.ea=c.track,e=this.cue=c.cue,g=a.currentTime();c.label=e.text;c.selected=e.startTime<=g&&g<e.xa;u.I.call(this,a,c);d.d("cuechange",u.bind(this,this.update))}});u.eb.prototype.q=function(){u.I.prototype.q.call(this);this.c.currentTime(this.cue.startTime);this.update(this.cue.startTime)};u.eb.prototype.update=function(){var a=this.cue,c=this.c.currentTime();this.selected(a.startTime<=c&&c<a.xa)};
u.l.B(u.Ha.prototype.j.children,{subtitlesButton:{},captionsButton:{},chaptersButton:{}});
if("undefined"!==typeof window.JSON&&"function"===window.JSON.parse)u.JSON=window.JSON;else{u.JSON={};var Z=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;u.JSON.parse=function(a,c){function d(a,e){var k,p,n=a[e];if(n&&"object"===typeof n)for(k in n)Object.prototype.hasOwnProperty.call(n,k)&&(p=d(n,k),p!==b?n[k]=p:delete n[k]);return c.call(a,e,n)}var e;a=String(a);Z.lastIndex=0;Z.test(a)&&(a=a.replace(Z,function(a){return"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)}));
if(/^[\],:{}\s]*$/.test(a.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,"")))return e=eval("("+a+")"),"function"===typeof c?d({"":e},""):e;throw new SyntaxError("JSON.parse(): invalid or malformed JSON data");}}
u.jc=function(){var a,c,d=document.getElementsByTagName("video");if(d&&0<d.length)for(var e=0,g=d.length;e<g;e++)if((c=d[e])&&c.getAttribute)c.player===b&&(a=c.getAttribute("data-setup"),a!==j&&(a=u.JSON.parse(a||"{}"),videojs(c,a)));else{u.sb();break}else u.Pc||u.sb()};u.sb=function(){setTimeout(u.jc,1)};"complete"===document.readyState?u.Pc=f:u.W(window,"load",function(){u.Pc=f});u.sb();u.Hd=function(a,c){u.Player.prototype[a]=c};var sa=this;sa.Yd=f;function $(a,c){var d=a.split("."),e=sa;!(d[0]in e)&&e.execScript&&e.execScript("var "+d[0]);for(var g;d.length&&(g=d.shift());)!d.length&&c!==b?e[g]=c:e=e[g]?e[g]:e[g]={}};$("videojs",u);$("_V_",u);$("videojs.options",u.options);$("videojs.players",u.Aa);$("videojs.TOUCH_ENABLED",u.fc);$("videojs.cache",u.ta);$("videojs.Component",u.a);u.a.prototype.player=u.a.prototype.m;u.a.prototype.options=u.a.prototype.options;u.a.prototype.init=u.a.prototype.h;u.a.prototype.dispose=u.a.prototype.dispose;u.a.prototype.createEl=u.a.prototype.e;u.a.prototype.contentEl=u.a.prototype.ia;u.a.prototype.el=u.a.prototype.w;u.a.prototype.addChild=u.a.prototype.V;
u.a.prototype.getChild=u.a.prototype.ja;u.a.prototype.getChildById=u.a.prototype.rd;u.a.prototype.children=u.a.prototype.children;u.a.prototype.initChildren=u.a.prototype.vc;u.a.prototype.removeChild=u.a.prototype.removeChild;u.a.prototype.on=u.a.prototype.d;u.a.prototype.off=u.a.prototype.p;u.a.prototype.one=u.a.prototype.W;u.a.prototype.trigger=u.a.prototype.k;u.a.prototype.triggerReady=u.a.prototype.Ea;u.a.prototype.show=u.a.prototype.show;u.a.prototype.hide=u.a.prototype.G;
u.a.prototype.width=u.a.prototype.width;u.a.prototype.height=u.a.prototype.height;u.a.prototype.dimensions=u.a.prototype.kd;u.a.prototype.ready=u.a.prototype.K;u.a.prototype.addClass=u.a.prototype.o;u.a.prototype.removeClass=u.a.prototype.r;u.a.prototype.buildCSSClass=u.a.prototype.S;u.Player.prototype.ended=u.Player.prototype.ended;$("videojs.MediaLoader",u.Yc);$("videojs.TextTrackDisplay",u.gc);$("videojs.ControlBar",u.Ha);$("videojs.Button",u.s);$("videojs.PlayToggle",u.bc);
$("videojs.FullscreenToggle",u.Ia);$("videojs.BigPlayButton",u.cb);$("videojs.LoadingSpinner",u.$b);$("videojs.CurrentTimeDisplay",u.fb);$("videojs.DurationDisplay",u.gb);$("videojs.TimeDivider",u.hc);$("videojs.RemainingTimeDisplay",u.nb);$("videojs.LiveDisplay",u.Zb);$("videojs.ErrorDisplay",u.hb);$("videojs.Slider",u.Q);$("videojs.ProgressControl",u.mb);$("videojs.SeekBar",u.dc);$("videojs.LoadProgressBar",u.jb);$("videojs.PlayProgressBar",u.ac);$("videojs.SeekHandle",u.Ka);
$("videojs.VolumeControl",u.pb);$("videojs.VolumeBar",u.ob);$("videojs.VolumeLevel",u.ic);$("videojs.VolumeMenuButton",u.qa);$("videojs.VolumeHandle",u.qb);$("videojs.MuteToggle",u.ha);$("videojs.PosterImage",u.Ja);$("videojs.Menu",u.ga);$("videojs.MenuItem",u.I);$("videojs.MenuButton",u.L);$("videojs.PlaybackRateMenuButton",u.cc);u.L.prototype.createItems=u.L.prototype.ua;u.U.prototype.createItems=u.U.prototype.ua;u.Ga.prototype.createItems=u.Ga.prototype.ua;$("videojs.SubtitlesButton",u.La);
$("videojs.CaptionsButton",u.Fa);$("videojs.ChaptersButton",u.Ga);$("videojs.MediaTechController",u.t);u.t.prototype.features=u.t.prototype.n;u.t.prototype.n.volumeControl=u.t.prototype.n.Oc;u.t.prototype.n.fullscreenResize=u.t.prototype.n.ce;u.t.prototype.n.progressEvents=u.t.prototype.n.ge;u.t.prototype.n.timeupdateEvents=u.t.prototype.n.je;u.t.prototype.setPoster=u.t.prototype.Qb;$("videojs.Html5",u.f);u.f.Events=u.f.ib;u.f.isSupported=u.f.isSupported;u.f.canPlaySource=u.f.ub;
u.f.patchCanPlayType=u.f.Ac;u.f.unpatchCanPlayType=u.f.Vd;u.f.prototype.setCurrentTime=u.f.prototype.Kd;u.f.prototype.setVolume=u.f.prototype.Qd;u.f.prototype.setMuted=u.f.prototype.Nd;u.f.prototype.setPreload=u.f.prototype.Pd;u.f.prototype.setAutoplay=u.f.prototype.Jd;u.f.prototype.setLoop=u.f.prototype.Md;u.f.prototype.enterFullScreen=u.f.prototype.oc;u.f.prototype.exitFullScreen=u.f.prototype.od;u.f.prototype.playbackRate=u.f.prototype.playbackRate;u.f.prototype.setPlaybackRate=u.f.prototype.Od;
$("videojs.Flash",u.i);u.i.isSupported=u.i.isSupported;u.i.canPlaySource=u.i.ub;u.i.onReady=u.i.onReady;$("videojs.TextTrack",u.C);u.C.prototype.label=u.C.prototype.label;u.C.prototype.kind=u.C.prototype.J;u.C.prototype.mode=u.C.prototype.mode;u.C.prototype.cues=u.C.prototype.fd;u.C.prototype.activeCues=u.C.prototype.$c;$("videojs.CaptionsTrack",u.Wb);$("videojs.SubtitlesTrack",u.ec);$("videojs.ChaptersTrack",u.Xb);$("videojs.autoSetup",u.jc);$("videojs.plugin",u.Hd);$("videojs.createTimeRange",u.zb);
$("videojs.util",u.oa);u.oa.mergeOptions=u.oa.Kb;})();




var mobile_scale=1;
$(function(){
	var body_size=$('body');
	var centrer1=$('#centrer1');
	var centrer2=$('#centrer2');
	var scroll1=centrer1.find('.scroll_frame');
	var scroll2=centrer2.find('.scroll_frame');
	var preloader=$('#preloader_frame');
	var ratio=0.5625;
	var apart_ratio=1;
	var paper_size, paper_size2, paper_size3, paper_size4;
	var time=700;
	var old_page;
	var page=body_size.attr('class');
	var frame_w, frame_h, frame_d, centrer1_w, centrer1_h;
	var easyIn='easeInQuart';
	var easyOut='easeOutQuart';
	var easyInOut='easeInOutQuart';	
	var ani=false;
	var mobile=false;
	var transitions_av=true;
	var pano_help=true;
	var xml_txt;
	var p_frame1, p_frame2, p_frame3, mouse_pos;
	var home_slides=['0015','0004'];
	var pages=[
		
	];
	var ani_names=[
		
	];	
	
	var popup_video, video_w, video_h, carousel, data, param_search;
	var carousel_scroll_av, carousel_line, carousel_items_num, carousel_items_targ;
	var bg_slides={};
	var plans_val={};
	plans_val['q']=1;
	
	
	
	if (!Modernizr.csstransitions || !Modernizr.cssanimations) {
		transitions_av=false;
		$.fn.transition=$.fn.animate;
		$.fn.transitionStop = $.fn.stop;
	}

	if(/iPad/i.test(navigator.userAgent) || /iPhone/i.test(navigator.userAgent) || /Android/i.test(navigator.userAgent))  {
		mobile=true;
		$('#body_frame').attr('class','mobile');
		centrer2.append('<div class="rotate_help_frame"><img src="/assets/i/rotate.png" class="rotate_help" /></div>');
		var rotate_help=centrer2.find('.rotate_help_frame');
	}
	if(mobile || !Modernizr.video) {
		ani=false;
	}
	 
	if(ani) {
		var bg_video, videoInt, videoTime, is_video, html_loaded;
		centrer1.prepend('<video id="bg_video" class="video-js" preload="auto"><source src="" type="video/webm" /><source src="" type="video/mp4" /></video>');
		videojs('bg_video', {}, function(){
			bg_video=this;
			bg_video.on('ended',function(){
				load_js(page,old_page);
			});
		});		
	} else {
		$('.ani_toggle').remove();
	}
	function content_move() {
		if(mobile) {
			var min_window_w=1250;
			var min_window_h=650;
			var min_window_d=min_window_h/min_window_w;
			var window_w=$(window).width();
			var window_h=$(window).height();
			var window_d=window_h/window_w;
			if(window_d<min_window_d) {
				var scale=Math.min(1,window_h/min_window_h);
				body_size.css({'min-width':window_w/scale,'min-height':min_window_h,'transformOrigin':'0 0','scale':scale});
			} else {
				var scale=Math.min(1,window_w/min_window_w);
				body_size.css({'min-width':min_window_w,'min-height':window_h/scale,'transformOrigin':'0 0','scale':scale});
			}
			if(rotate_help) {
				if(window_h>window_w) {
					rotate_help.show();
				} else {
					rotate_help.hide();
				}
			}
		}
		frame_w=body_size.width();
		frame_h=body_size.height();
		frame_d=frame_h/frame_w;
		if(frame_d>ratio) {
			centrer1_w=frame_h/ratio;
			centrer1_h=frame_h;
			centrer1.css({'width':centrer1_w,'height':centrer1_h,'top':0,'left':0.5*(frame_w-centrer1_w)});
		} else {
			centrer1_w=frame_w;
			centrer1_h=frame_w*ratio;
			centrer1.css({'width':centrer1_w,'height':centrer1_h,'top':0.5*(frame_h-centrer1_h),'left':0});
		}
		var menu_h=Math.round(frame_h*0.0866);
		$('.footer_frame').css({'height':Math.round(frame_h*0.2122)})
		$('.menu_frame').css({'bottom':menu_h})
			.find('.menu_a').css({'line-height':menu_h+'px'}).end()
			.find('.submenu_frame').css({'padding-bottom':menu_h});
		if(paper_size) {
			paper_size.changeSize(centrer1_w, centrer1_h, true, false);
		}
		if(paper_size4) {
			plan_check_size();
        }
		if(popup_video) {
			popup_video_reload();
		}		
	}
	function plan_check_size() {
		var fr=centrer2.find('.plan_frame_centrer:last');
		var fr2=fr.parent();
		var plan_size=Math.min(fr2.width()/apart_ratio,fr2.height());
		fr.css({'width':plan_size*apart_ratio,'height':plan_size,'margin-top':-0.5*plan_size,'margin-left':-0.5*plan_size*apart_ratio});
		if(paper_size2) {
			paper_size2.changeSize(plan_size*apart_ratio, plan_size, true, false);
			paper_size3.changeSize(plan_size*apart_ratio, plan_size, true, false);
		}
	}
	$(window).bind('resize',function(){
		content_move();
	});
	content_move();
	setTimeout(function(){content_move();},500);
	setTimeout(function(){content_move();},1500);
	setTimeout(function(){content_move();},2500);
	
	function test_json(targ_function) {
		if(!data) {
			$.ajax({
				url: '/assets/js/data.json',
				dataType: 'json',
				success: function(response){
					data=response;
					//console.log('loaded json');
					targ_function();
				}
			});
		} else {
			//console.log('already loaded json');
			targ_function();
		}		
	}
	var load_js=function(targ,prev) {
		//console.log('load_js '+targ);
		var menu_targ=targ;
		body_size.attr('class',targ);		
		var frame1=scroll1.find('.'+targ+'_frame:last');
		var frame2=scroll2.find('.'+targ+'_frame:last');
		var prev_frame1=frame1.prevAll();
		var prev_frame2=frame2.prevAll();		
		var dir;
		var prev_index=$.inArray(prev,pages);
		var next_index=$.inArray(targ,pages);
		function frame1_ani_ended(){
			scroll1.removeAttr('style');
			frame1.css({'left':0});
			prev_frame1.remove();
		}			
		function frame2_ani_ended(){
			scroll2.removeAttr('style');
			frame2.css({'left':0});
			prev_frame2.remove();
		}
		if(prev_index!=-1 && next_index!=-1) {
			if(prev_index<next_index) {
				dir=1;
			} else {
				dir=-1;
			}
		}
		if(ani && $.inArray(prev+'-'+targ,ani_names)!=-1) {
			frame1.css({'left':0})
				.children(':not(.bg_img)').css({'opacity':0}).transition({'opacity':1},time);
			frame2.css({'left':0,'opacity':0}).transition({'opacity':1},time);			
			prev_frame1.remove();
			prev_frame2.remove();
		} else
		if(dir) {			
			frame1.css({'left':(100*dir)+'%'});
			frame2.css({'left':(100*dir)+'%'});	
			if(transitions_av) {
				scroll1.delay(100).css({'translate':0}).transition({'translate':(-100*dir)+'%'},time,function(){
					frame1_ani_ended();
				});
				scroll2.delay(100+time*0.3).css({'translate':0}).transition({'translate':(-100*dir)+'%'},time,function(){
					frame2_ani_ended();
				});
			} else {
				scroll1.delay(100).animate({'left':(-100*dir)+'%'},time,function(){
					frame1_ani_ended();
				});
				scroll2.delay(100+time*0.3).animate({'left':(-100*dir)+'%'},time,function(){
					frame2_ani_ended();
				});
			}
		} else {
			frame1.css({'left':0,'opacity':0}).transition({'opacity':1},time,function(){
				frame1_ani_ended();	
			});
			frame2.css({'left':0,'opacity':0}).transition({'opacity':1},time,function(){
				frame2_ani_ended();	
			});
			prev_frame1.stop(true).transition({'opacity':0},time);
			prev_frame2.stop(true).transition({'opacity':0},time);
		}
		if(ani) {
			videoTime=setTimeout(function(){
				$('#bg_video').hide();
				bg_video.src('');
			},100);
		}
		
		frame2.find('.text_scroll').removeClass('browser_scroll').jScrollPane({showArrows:false,autoReinitialise:true,verticalDragMinHeight:35,verticalDragMaxHeight:35});
		
		if(targ=='home') {
			bg_slides['slides']=home_slides;
			bg_slides['av']=true;
			bg_slides['time']=7500;
			bg_slides['frame']=frame1;
			load_bg_slide(0);
			frame2.find('.bg_slides_frame, .bg_slides_dots_frame')
				.bind('mouseenter',function(){
					stop_bg_slide();
				})
				.bind('mouseleave',function(){
					bg_slides['av']=true;
					start_bg_slide();
				})
			$('.logo_bg').transition({'rotate':-380,'scale':1.2},1000);
			frame2.find('.home_links_frame>*').each(function(){
				scale_show($(this),time+500+$(this).index()*700);
			})
			scale_show(frame2.find('.actions_banner'),time+1200);
		} else
		if(targ=='about') {
			scale_show(frame2.find('.to_gallery'),time+500);
			parallax_init(frame1);
		} else
		if(targ=='advantages') {
			adv_pos(0,0);
			parallax_init(frame1);
			//frame2.find('.adv_icons').css({'translate3d':0})
		} else
		if(targ=='company') {
			parallax_init(frame1);
		} else
		if(targ=='documents') {
			parallax_init(frame1);
		} else
		if(targ=='construction') {
			init_carousel(frame2,1);
			make_constr_active(0,0)
			frame2.find('.contr_bg2').css({'rotate':270,'opacity':0}).delay(time).transition({'rotate':0,'opacity':1},2000);
		} else
		if(targ=='placement' || targ=='infrastructure') {
			frame2.find('.map_place').attr('id','map_place');			
			frame2.find('.place_btns_frame>*').each(function(){
				scale_show($(this),time+500+$(this).index()*700);
			})
			$.ajax({
				//url: '/assets/pages/load.php?url=LbZ-jWocVYO9ztHCziBjd_t48Au1gCVi',
				url: '/assets/files/infra.xml',
				cache: true,
				dataType: 'xml',
				type: 'GET',
				success: function(response){
					xml_txt=[];
					var hrefs={'pmlbs.png':0,'pmbls.png':1,'pmdbs.png':2,'pmgns.png':3,'pmnts.png':4,'pmpns.png':5,'pmvvs.png':6,'pmyws.png':7,'pmors.png':8,'pmdos.png':9,'pmrds.png':10,'pmgrs.png':11,'pmwts.png':12};
					$(response).find('Placemark').each(function() {
						var point_item=$(this);
						var point_coord=point_item.find('coordinates').text().split(',');
						var num=hrefs[point_item.find('href').text().replace('http://api-maps.yandex.ru/i/0.4/micro/','')];
						//if(num==10 || page=='infrastructure') {
							xml_txt.push([Number(point_coord[1]),Number(point_coord[0]),point_item.find('name').text(),point_item.find('description').text(),num]);	
						//}
					});					
					load_placement_map(true);
				}
			})
		} else
		if(targ=='gallery') {
			var gallery_frame1=frame1.find('.gallery_place');
			var gallery_frame2=frame2.find('.gallery_place');
			var gallery_images=gallery_frame1.data('targ').split(',');
			carousel=gallery_frame1.galleryInit({
				frame2:gallery_frame2,
				path:gallery_frame1.data('path'),
				images:gallery_images,
				start_num:gallery_frame1.data('start'),
				preview_width:120,			
				time:600,
				mobile:mobile,
				force3d:transitions_av,
				afterMove:function(num){
					body_size.load_content('/gallery/'+gallery_images[num],{'suppress_load':true});
				},
				loadComplete:function(){
					frame1.find('.bg_img').remove();
					var active_num=gallery_frame2.find('.g_p.active').data('targ');
					gallery_frame2.find('.g_p').each(function(){
						$(this).delay(time+150*Math.abs($(this).data('targ')-active_num)).transition({'top':0},400,easyOut);	
					})
				}
			});	
		} else
		if(targ=='news' || targ=='actions') {
			init_carousel(frame2,3);
			if(old_page!='news-article') {
				frame2.find('.carousel_item').each(function(){
					var num=$(this).index();
					if(num<3) {
						$(this).css({'opacity':0,'scale':1.2}).delay(time+200*num).transition({'opacity':1,'scale':1},500);
					}
				})
			}
		} else
		if(targ=='news-article') {
			menu_targ='news';
			frame2.find('.fancybox').fancybox({padding:0,margin:[75,100,40,100]});
		} else
		if(targ=='actions-article') {
			menu_targ='actions';
		} else
		if(targ=='banks') {
			menu_targ='ipoteque';
		} else
		if(targ=='calculator') {
			menu_targ='ipoteque';
			function get_calc(){
				var val1=Number(frame2.find('input.n1').val().replace(/\s/g,''));
				var val2=Number(frame2.find('input.n2').val()/100);
				var val3=Number(frame2.find('input.n3').val());				
				var result=0;
				if(val1 && val2 && val3 && val3>2) {
					result=Math.round(val1*((val2/12)/(1-Math.pow((1+(val2/12)),(-(val3-2))))));
				}
				frame2.find('.calc_result>span').text(addspace(result));
			}
			get_calc();
			frame2.find('input').bind('change input',function(){				
				var val=$(this).val().replace(/\s+/g,'');
				var new_val=val;				
				if($(this).hasClass('n1')) {
					new_val=new_val.replace(/[^\d]/,'');
					new_val=addspace(new_val);
				} else
				if($(this).hasClass('n2')) {
					new_val=new_val.replace(',','.');
					new_val=new_val.replace(/[^\.\d]/,'');
				} else
				if($(this).hasClass('n3')) {
					new_val=new_val.replace(/[^\d]/,'');
				}
				$(this).val(new_val);
				get_calc();
			});
		} else
		if(targ=='vtour') {
			//drag_point($('.pano_points_frame'),true,false,false,true);
			frame2.find('.pano_place')
				.css({'cursor':'url(/assets/i/cur1.cur), move','translate3d':0})
				.bind('mousedown',function(){
					$(this).css({'cursor':'url(/assets/i/cur2.cur), move'})
				})
				.bind('mouseup',function(){
					$(this).css({'cursor':'url(/assets/i/cur1.cur), move'})
				})
			frame1.find('.pano_open_btn').sortY().each(function(){
				$(this).css({'margin-top':-105,'opacity':0}).delay(time+2000*Math.random()).animate({'margin-top':-55,'opacity':1},700,'easeOutBounce');	
			})
		} else
		if(targ=='plans') {
			frame2.find('.home_links_frame>*').each(function(){
				scale_show($(this),time+500+$(this).index()*700);
			})
			test_json(function(){
				load_plans_map(frame1,frame2);
				frame1.find('.korpus_icon').each(function(){
					var num=$(this).data('targ');
					if(data.buildings[num])	{
						var at=data.buildings[num];
						$(this).addClass('pjax').attr('href','/plans/korpus'+num)
							.find('.korpus_icon_popup').html('<div class="korpus_icon_popup_bg"><div class="korpus_icon_val">'+at.at+'</div><div class="korpus_icon_text">РєРІР°СЂС‚РёСЂ'+word_end(at.at)+'<div>РІ РїСЂРѕРґР°Р¶Рµ</div></div></div><div class="korpus_icon_av n0">СЃС‚СѓРґРёР№<span>'+at.arc[0]+'</span></div><div class="korpus_icon_av n1">1-РєРѕРјРЅР°С‚РЅС‹С…<span>'+at.arc[1]+'</span></div><div class="korpus_icon_av n2">2-РєРѕРјРЅР°С‚РЅС‹С…<span>'+at.arc[2]+'</span></div><div class="korpus_icon_av n3">3-РєРѕРјРЅР°С‚РЅС‹С…<span>'+at.arc[3]+'</span></div><div class="korpus_icon_av n4">4-РєРѕРјРЅР°С‚РЅС‹С…<span>'+at.arc[4]+'</span></div>');
						paper_size.getByAlt(num).data('av',1);	
					} else {
						var txt='РЅРµС‚ РІ РїСЂРѕРґР°Р¶Рµ';
						if(num==4) {
							txt='СЃРєРѕСЂРѕ РІ РїСЂРѕРґР°Р¶Рµ';
						}
						$(this).addClass('soon')
							.find('.korpus_icon_popup').html('<div class="korpus_icon_popup_bg"><div class="korpus_icon_val">'+txt+'</div></div>');
						paper_size.getByAlt(num).data('av',0).attr({'cursor':'default'});
					}
				})
			})
			//drag_point($('.pano_points_frame'),true,false,false,true);
		} else
		if(/section/.test(targ)) {
			menu_targ='plans';
			frame2.find('.home_links_frame>*').each(function(){
				scale_show($(this),time+500+$(this).index()*700);
			})
			test_json(function(){
				load_section_map(frame1,frame2);
			})
			/*if(old_page=='korpus2' || old_page=='korpus2-back') {
				frame2.find('.plans_back').attr('href','/plans/'+old_page);	
			}*/
		} else
		if(/korpus/.test(targ)) {
			menu_targ='plans';
			frame2.find('.home_links_frame>*').each(function(){
				scale_show($(this),time+500+$(this).index()*700);
			})
			test_json(function(){
				load_korpus_map(frame1,frame2);
			})
		} else
		if(targ=='search') {
			test_json(function(){
				param_search=centrer2.find('.load_frame:last').searchInit({
					data:data.apartments,
					scroll_height:35,
					pagination:0,
					loadAnimate:function(el){
						//el.css({'scale':0.9,'opacity':0}).transition({'scale':1,'opacity':1},500);					
					}/*,
					resultClick:function(id){
						var d=data.apartments[id];
						body_size.load_content('/plans/korpus'+d.b+'/section'+d.s+'/floor'+d.f+'/flat'+d.n);	
					}*/
				});
			})
			if(old_page=='korpus') {				
				frame2.find('.close_btn').attr('href','/plans/korpus'+plans_val['b']);	
			} else			
			if(old_page=='section') {				
				frame2.find('.close_btn').attr('href','/plans/korpus'+plans_val['b']+'/section'+plans_val['s']);	
			}
		} else
		if(targ=='contacts') {
			frame2.find('.map_place').attr('id','map_place');
			load_placement_map(false);
			scale_show(frame2.find('.open_route'),time+500);
			scale_show(frame2.find('.open_road'),time+700);
			frame2.find('.contr_bg2').css({'rotate':270,'opacity':0}).delay(time).transition({'rotate':0,'opacity':1},2000);
		} else
		if(targ=='buy') {
			frame2.find('.buy_icons').each(function(){
				scale_show($(this),time+$(this).index()*400);
			})
		} else
		if(targ=='ipoteque') {
			frame2.find('.buy_icons').each(function(){
				scale_show($(this),time+$(this).index()*400);
			})
		} else
		if(targ=='instalment') {
			
		}
		
		
		
		$('.menu')
			.find('.active').removeClass('active').end()
			//.find('.menu_a.'+menu_targ+'_link, .submenu_a.'+submenu_targ+'_link').addClass('active');	
			.find('.menu_a.'+menu_targ+'_link').addClass('active').parent().addClass('active').end().end()
			.find('.submenu_a.'+menu_targ+'_link').addClass('active').parent().parent().addClass('active');
		old_page=targ;
	}
	function unload_js(targ,next) {		
		//console.log('unload_js '+targ);
		var frame1=scroll1.find('.'+targ+'_frame:last');
		var frame2=scroll2.find('.'+targ+'_frame:last');
		
		if(targ=='home') {
			frame2.find('.bg_slides_frame, .bg_slides_dots_frame').unbind('mouseenter mouseleave');
			stop_bg_slide();
			$('.logo_bg').transition({'rotate':0,'scale':1},1000);
		}
		
		if(paper_size) {
			paper_size=null;
			paper_size2=null;
			paper_size3=null;
		}
		if(carousel) {
			carousel.removeEvents();
			carousel=null;
		}
		if(param_search) {
			param_search.removeEvents();
			param_search=null;
		}
		if(frame2.find('.map_place').length) {
			frame2.find('.map_place').removeAttr('id');	
		}
		p_frame1=null;
		p_frame2=null;
		p_frame3=null;
	}
	function add_stat(url) {
		//console.log('add_stat '+url);
		ga('send', 'pageview', url);
		//_gaq.push(['_trackPageview', url]);
		yaCounter29172190.hit(url);
	}
	function parallax_init(frame) {
		p_frame1=frame.find('.p_item.n1');
		p_frame2=frame.find('.p_item.n2');
		p_frame3=frame.find('.p_item.n3');
		move_bg();
	}
	function move_bg() {		
		if(mouse_pos) {
			var x=mouse_pos.pageX/mobile_scale;
			var y=mouse_pos.pageY/mobile_scale;
			var proc=x/frame_w;
			var pos1=3*(0.5-proc)+'%';
			var pos2=6*(0.5-proc)+'%';
			var pos3=9*(0.5-proc)+'%';
			if(transitions_av==true) {
				if(p_frame1) {
					p_frame1.css({translate3d:pos1});
				}
				if(p_frame2) {
					p_frame2.css({translate3d:pos2});
				}
				if(p_frame3) {
					p_frame3.css({translate3d:pos3});
				}
			} else {
				if(p_frame1) {
					p_frame1.css({'left':pos1});
				}
				if(p_frame2) {
					p_frame2.css({'left':pos2});
				}
				if(p_frame1) {
					p_frame3.css({'left':pos3});
				}
			}
		}
	}
	
	/* СЃР»Р°Р№РґС€РѕСѓ РЅР° РіР»Р°РІРЅРѕР№ */
	
	function start_bg_slide() {
		bg_slides['timeout']=setTimeout(function(){
			load_bg_slide(bg_slides['num']+1);
		},bg_slides['time']);
	}
	function stop_bg_slide() {
		bg_slides['av']=false;
		clearTimeout(bg_slides['timeout']);	
	}
	function load_bg_slide(num) {

		clearTimeout(bg_slides['timeout']);		
		if(num>bg_slides['slides'].length-1) {
			num=0;	
		} else
		if(num<0) {
			num=bg_slides['slides'].length-1;
		}
		if(bg_slides['av']) {
			start_bg_slide();
		}
		var fr=bg_slides['frame'];
		if(fr.find('.bg_img:last').data('targ')!=bg_slides['slides'][num]) {
			load_bg_img(fr,bg_slides['slides'][num]);			
		}
		$('.bg_slides_frame')
			.find('.bg_slides_item.active').stop(true).transitionStop().removeClass('active').transition({'opacity':0,'scale':1.1},300,function(){
				$(this).css({'display':'none'});	
			}).end()
			.find('.bg_slides_item.n'+num).stop(true).transitionStop().addClass('active').css({'display':'block','opacity':0,'scale':0.9}).delay(300).transition({'opacity':1,'scale':1},300);
		$('.bg_slides_dots_frame').find('.bg_slides_dots[data-targ="'+num+'"]').addClass('active').siblings().removeClass('active');
		bg_slides['num']=num;
	}
	function load_bg_img(frame,name) {
		frame
			.append('<img class="bg_img n'+name+'" data-targ="'+name+'" src="/uploads/'+name+'.jpg" />')
			.find('.bg_img:last').css({'opacity':0,'scale':1.1,'translate3d':0}).load(function(){
				$(this).transition({'opacity':1,'scale':1},1000,function(){
					$(this).prevAll('.bg_img').remove();
				});
			});
	}
	
	
	function random_num(slides) {
		var r=slides[Math.floor(Math.random()*slides.length)];
		if (r!=slideshow_slide) {
			return r;
		} else {
			return random_num(slides);
		}
	}
	function scale_show(targ,delay) {
		targ.delay(delay).css({'scale':0.7,'opacity':0,'display':'block'}).transition({'scale':1.2,'opacity':1},400).transition({'scale':1},500);
	}
	
	Number.prototype.toRad = function () { return this * Math.PI / 180; }
	function adv_pos(num,time) {
		var fr=$('.adv_icons_frame');
		fr.find('.adv_icons').each(function(){
			var item_index=$(this).data('targ');			
			var delta=0;
			if(item_index>num) {
				delta=1;	
			} else
			if(item_index<num) {
				delta=-1;	
			}
			var pos=item_index-num+0.7*delta;
			var angle=(180-(pos*13)).toRad();
			var radius=50;
			var new_css={};
			new_css['left']=50+radius*Math.cos(angle)+'%';
			new_css['top']=50+radius*Math.sin(angle)+'%';
			if($(this).hasClass('active')) {
				$(this).removeClass('active');
				new_css['width']=80;
				new_css['height']=80;
				new_css['margin-top']=-40;
				new_css['margin-left']=-40;
			} else
			if(item_index==num) {
				$(this).addClass('active');
				new_css['width']=120;
				new_css['height']=120;
				new_css['margin-top']=-60;
				new_css['margin-left']=-60;
			}
			$(this).transitionStop(true).transition(new_css,time);
		})
		//console.log('---------')
		if(time) {
			var scroll_frame=scroll2.find('.advantages_frame .text_scroll');
			scroll_frame.transitionStop(true).transition({'opacity':0},0.5*time,function(){
				$(this).find('.jspPane>div').load('/about/advantages/page'+num,function(){
					scroll_frame.transition({'opacity':1},0.5*time);
					scroll_frame.data('jsp').scrollTo(0,0);
					scroll_frame.data('jsp').reinitialise();	
				})	
			});
			load_bg_img(scroll1.find('.advantages_frame .p_item'),home_slides[fr.find('.active').data('targ')]);
		}
	}
	
	/* РїРѕРї-Р°Рї СЃ РІРёРґРµРѕ */
	
	function open_popup_video(name) {
		if(!$('.video_popup').length) {
			centrer2.append('<div class="popup_overlay video_popup"></div>');
				var fr=centrer2.find('.video_popup');
				fr.html('<div class="video_popup_center">'+name+'<div class="close_btn video_close"></div></div>').css({'opacity':0,'display':'block'});
				video_w=fr.find('video').attr('width');
				video_h=fr.find('video').attr('height');
				videojs('popup_video', {}, function(){
					popup_video=this;
					popup_video_reload();	
					fr.transition({'opacity':1},300);
				})
		}
	}	
	function unload_popup_video() {
		if(popup_video) {
			popup_video.src('');				
			popup_video.dispose();
			popup_video=null;
		}
	}
	function popup_video_reload() {
		var cur_video_w=Number(video_w);
		var cur_video_h=Number(video_h);
		var video_frame_w=Number(frame_w)-150;
		var video_frame_h=Number(frame_h)-150;	
		if(cur_video_w>video_frame_w || cur_video_h>video_frame_h) {
			var scale=Math.min(video_frame_w/video_w,video_frame_h/video_h);
			cur_video_w=video_w*scale;
			cur_video_h=video_h*scale;
		}		
		centrer2.find('.video_popup_center').css({'margin-top':-0.5*cur_video_h,'margin-left':-0.5*cur_video_w});		
		popup_video.width(cur_video_w);
		popup_video.height(cur_video_h);
	}
	function load_road_text(num,time) {
		var fr=$('.road_frame');
		var text_scroll=fr.find('.text_scroll');
		fr.find('.road_sel.n'+num).addClass('active').siblings().removeClass('active');
		text_scroll.transitionStop(true).transition({'opacity':0},time,function(){
			$(this).find('.jspPane>div').load('/contacts/'+num,function(){
				text_scroll.transition({'opacity':1},time);
				text_scroll.data('jsp').scrollTo(0,0);
				text_scroll.data('jsp').reinitialise();
			})	
		})
	}
	
	/* РґРёРЅР°РјРёРєР° - СЃР»Р°Р№РґРµСЂ */
	
	function init_carousel(frame,item_n) {	
		carousel_scroll_av=true;
		carousel_line=frame.find('.carousel_line');
		carousel_items_num=carousel_line.children('.carousel_item').length;
		carousel_items_targ=item_n;
		var dots_num=Math.ceil(carousel_items_num/carousel_items_targ);
		if(dots_num>1) {
			var txt='<div class="carousel_dots_frame">';
			for(var i=0; i<dots_num; i++) {
				txt+='<div class="carousel_dots'+(i==0?' active':'')+'" data-targ="'+i+'"></div>';
			}
			txt+='</div><div class="carousel_arrow left"></div><div class="carousel_arrow right"></div>';
			frame.find('.carousel_frame').prepend(txt);
			txt=null;
		} else {
			carousel_line.css({'left':0.5*carousel_line.children(':first').outerWidth(true)*(carousel_items_targ-carousel_items_num)});
		}
		carousel_line.css({'translate3d':0});
	}
	function make_constr_active(targ_data,time) {
		carousel_line.find('.active').removeClass('active').stop(true).transitionStop().transition({'width':170},time)
			.find('.carousel_item_img').stop(true).transitionStop().transition({'margin-top':-85,'margin-left':-85},time).end()
			.find('.carousel_item_title').stop(true).transitionStop().transition({'opacity':0},time,function(){
				$(this).css({'display':'none'});	
			});
		carousel_line.find('.carousel_item').eq(targ_data).addClass('active').stop(true).transitionStop().transition({'width':244},time)
			.find('.carousel_item_img').stop(true).transitionStop().transition({'margin-top':-122,'margin-left':-122},time).end()
			.find('.carousel_item_title').stop(true).transitionStop().css({'display':'block','opacity':0}).transition({'opacity':1},time);
	}
	
	/* РїР°РЅРѕСЂР°РјС‹ */		
	
	function load_pano(targ) {
		scroll2.find('.pano_place').stop(true).css({'opacity':0,'display':'block'}).delay(500).transition({'opacity':1},800).html('<div id="pano_screen"><script>embedpano({swf:"/assets/pages/pano/pano.swf",xml:"/assets/pages/pano/Pano_00'+targ+'.xml",target:"pano_screen",wmode:"opaque","bgcolor":"#000000",html5:"prefer"});</script></div><div class="shadow"></div><div class="close_btn pano_close"></div><div class="pano_help"></div>');
		if(pano_help) {
			$('.pano_help, .pano_place .shadow').delay(1500).fadeIn(300).delay(4000).fadeOut(300);
			pano_help=false;
		}
	}
	function unload_pano() {
		scroll2.find('.pano_place').stop(true).transition({'opacity':0},500,function(){
			$(this).hide().html('');
		});
	}
	function drag_point(frame,top,right,bottom,left) {
		var w, h;
		frame.children()
			.drag('start',function(){
				w=frame.width();
				h=frame.height();
			})
			.drag(function (ev,dd) {			
				if(top) $(this).css({'top':(100*dd.offsetY/h).toFixed(1)+'%'});
				if(right) $(this).css({'right':(100-100*dd.offsetX/w).toFixed(1)+'%'});
				if(bottom) $(this).css({'bottom':(100-100*dd.offsetY/h).toFixed(1)+'%'});
				if(left) $(this).css({'left':(100*dd.offsetX/w).toFixed(1)+'%'});
			},{relative:true})
			.bind('click',function(){
				return false;
			});
	}
	
	/* РїРѕРї-Р°Рї РѕР±СЂР°С‚РЅРѕР№ СЃРІСЏР·Рё */	
	
	function load_feedback(targ) {
		if(!$('.feedback_frame').length) {
			centrer2.append('<div class="popup_overlay feedback_popup"></div>')
				.find('.feedback_popup').load('/assets/pages/feedback'+targ+'.html',function(){
					$(this).css({'opacity':0,'display':'block'}).transition({'opacity':1},400)
						.find('input, textarea').placeholder().bind('change',function(){
							$(this).removeClass('error');	
						})
							.filter('.phone_input').mask('+7 (999) 999-99-99');
				})
		}
	}
	
	/* РєРІР°СЂС‚РёСЂС‹ */
	
	function load_plans_map(frame1,frame2) {
		paper_size=frame1.find('.plans_map').area2svg({
			'opacity':0,
			'fill':'#f29228',
			'fill-opacity':1,
			'stroke-opacity':0,
			onclick:function(el) {
				var alt=el.data('alt');
				if(el.data('av')) { 
					body_size.load_content('/plans/korpus'+alt);
				}
			},
			onmouseover:function(el) {
				var alt=el.data('alt');
				frame1.find('.korpus_icon.n'+alt).trigger('mouseenter').addClass('hover');
			},
			onmouseout:function(el) {
				var alt=el.data('alt');
				frame1.find('.korpus_icon.n'+alt).trigger('mouseleave').removeClass('hover');
			}
		})
		content_move();
	}
	function load_korpus_map(frame1,frame2) {
		plans_val['b']=Number(frame2.find('.korp_det.n0 .korp_det_val>div').text());
		var floor_popup=frame1.find('.korpus_popup');
		paper_size=frame1.find('.plans_map').area2svg({			
			'fill':'#f29228',
			'opacity':0,
			'stroke-opacity':0,
			'cursor':'default',
			onclick:function(el) {
				if(el.data('av')) {
					var alt=el.data('alt');
					body_size.load_content('/plans/korpus'+plans_val['b']+'/section'+alt);
				}
			},
			onmouseover:function(el) {
				var alt=el.data('alt');
				var box=el.getBBox();
				var scale=el.paper.width/frame1.find('.plans_map').attr('width');
				floor_popup.css({'display':'block','top':box.y*scale,'left':box.cx*scale})						
					.find('.val').text(alt);
				if(floor_popup.offset().top<5) {
					floor_popup.offset({'top':5});
				}
				if(el.data('av')) {
					el.attr({'opacity':0.8});
					var at=data.sections[plans_val['b']+'-'+alt];
					floor_popup.find('.korpus_icon_popup').html('<div class="korpus_icon_popup_bg"><div class="korpus_icon_val">'+at.at+'</div><div class="korpus_icon_text">РєРІР°СЂС‚РёСЂ'+word_end(at.at)+'<div>РІ РїСЂРѕРґР°Р¶Рµ</div></div></div><div class="korpus_icon_av n0">СЃС‚СѓРґРёР№<span>'+at.arc[0]+'</span></div><div class="korpus_icon_av n1">1-РєРѕРјРЅР°С‚РЅС‹С…<span>'+at.arc[1]+'</span></div><div class="korpus_icon_av n2">2-РєРѕРјРЅР°С‚РЅС‹С…<span>'+at.arc[2]+'</span></div><div class="korpus_icon_av n3">3-РєРѕРјРЅР°С‚РЅС‹С…<span>'+at.arc[3]+'</span></div><div class="korpus_icon_av n4">4-РєРѕРјРЅР°С‚РЅС‹С…<span>'+at.arc[4]+'</span></div>');
				} else {
					el.attr({'opacity':0.4});
					floor_popup.find('.korpus_icon_popup').html('');
				}
			},
			onmouseout:function(el) {				
				if(el.data('av')) {
					el.attr({'opacity':0.4});
				} else {
					el.attr({'opacity':0});
				}
				floor_popup.css({'display':'none'});
			},
			each:function(el) {
				el.data('av',0);
				var alt=el.data('alt');
				if(data.sections[plans_val['b']+'-'+alt]) {
					var at=data.sections[plans_val['b']+'-'+alt].at;
					if(at!=0) {
						el.attr({'opacity':0.4,'cursor':'pointer'}).data('av',at);
					}
				}
			}
		});
		content_move();				
	}
	function load_minimap_map(frame2) {
		frame2.find('.minimap_map:last').area2svg({	
			'opacity':0,		
			'fill':'#f29228',
			'fill-opacity':1,
			'stroke-opacity':0,
			'cursor':'default',
			onclick:function(el) {
				var alt=el.data('alt').split('-');
				if(!el.data('active') && el.data('av')) {
					body_size.load_content('/plans/korpus'+alt[0]+'/section'+alt[1]);
				}
			},
			onmouseover:function(el) {
				if(!el.data('active') && el.data('av')) {
					el.attr({'opacity':1});
				}
			},
			onmouseout:function(el) {	
				if(!el.data('active')) {
					el.attr({'opacity':0});
				}	
			},
			each:function(el) {
				var alt=el.data('alt').split('-');
				if(alt[1]==plans_val['s']) {
					el.attr({'opacity':1}).data('active',true);
				}
				el.data('av',0);
				if(data.sections[alt[0]+'-'+alt[1]]) {
					el.attr({'cursor':'pointer'}).data('av',1);	
				}
			}
		});
	}
	function load_section_map(frame1,frame2) {
		plans_val['b']=Number(frame2.find('.korp_det.n0 .korp_det_val>div').text());
		plans_val['s']=Number(frame2.find('.korp_det.n1 .korp_det_val>div').text());
		plans_val['floor_frame']=frame2.find('.floor_frame');
		plans_val['sect_sel']=frame2.find('.korp_det.n1');
		plans_val['floor_sel']=frame2.find('.korp_det.n2');
		plans_val['apart_details']=frame2.find('.apart_details_frame');			
		plans_val['floor_frame'].find('.minimap_frame').load('/assets/pages/minimaps/'+plans_val['b']+'.html',function(){
			load_minimap_map(frame2);
		})
		var floor_popup=frame1.find('.korpus_popup');
		if(frame1.hasClass('opened_floor') || frame1.hasClass('opened_flat')) {
			plans_val['f']=Number(plans_val['floor_sel'].find('.korp_det_val>div').text());
			if(frame1.hasClass('opened_floor')) {
				load_floor(0,true);
			} else
			if(frame1.hasClass('opened_flat')) {
				var flat_num=plans_val['b']+'-'+plans_val['apart_details'].find('.n1 .val').text();
				load_floor_details();
				load_apart_details(flat_num);
				load_apart(flat_num,0,true);				
				paper_size4=true;
				plan_check_size();
			}			
			$('.korp_det.n1').addClass('small');
		}
		if(!data.sections[plans_val['b']+'-'+(plans_val['s']-1)]) {
			plans_val['sect_sel'].find('.sect_left').remove();
		} else {
			plans_val['sect_sel'].find('.sect_left').attr('href','/plans/korpus'+plans_val['b']+'/section'+(plans_val['s']-1));	
		};
		if(!data.sections[plans_val['b']+'-'+(plans_val['s']+1)]) {
			plans_val['sect_sel'].find('.sect_right').remove();
		} else {
			plans_val['sect_sel'].find('.sect_right').attr('href','/plans/korpus'+plans_val['b']+'/section'+(plans_val['s']+1));	
		};
		paper_size=frame1.find('.plans_map').area2svg({			
			'fill':'#f29228',
			'opacity':0,
			'stroke-opacity':0,
			onclick:function(el) {
				var alt=el.data('alt');
				open_floor_popup(alt);					
				floor_popup.css({'display':'none'});
			},
			onmouseover:function(el) {
				var alt=el.data('alt');		
				if(el.data('av')) {
					el.attr({'opacity':0.8});
				} else {
					el.attr({'opacity':0.4});
				}
				var box=el.getBBox();
				var scale=el.paper.width/frame1.find('.plans_map').attr('width');
				var at=data.floors[plans_val['b']+'-'+alt];
				floor_popup.css({'display':'block','top':box.y*scale,'left':box.cx*scale})						
					.find('.val').text(alt.split('-')[1]).end()
					.find('.korpus_icon_popup').html('<div class="korpus_icon_popup_bg"><div class="korpus_icon_val">'+at.at+'</div><div class="korpus_icon_text">РєРІР°СЂС‚РёСЂ'+word_end(at.at)+'<div>РІ РїСЂРѕРґР°Р¶Рµ</div></div></div><div class="korpus_icon_av n0">СЃС‚СѓРґРёР№<span>'+at.arc[0]+'</span></div><div class="korpus_icon_av n1">1-РєРѕРјРЅР°С‚РЅС‹С…<span>'+at.arc[1]+'</span></div><div class="korpus_icon_av n2">2-РєРѕРјРЅР°С‚РЅС‹С…<span>'+at.arc[2]+'</span></div><div class="korpus_icon_av n3">3-РєРѕРјРЅР°С‚РЅС‹С…<span>'+at.arc[3]+'</span></div><div class="korpus_icon_av n4">4-РєРѕРјРЅР°С‚РЅС‹С…<span>'+at.arc[4]+'</span></div>');
			},
			onmouseout:function(el) {
				if(el.data('av')) {
					el.attr({'opacity':0.4});
				} else {
					el.attr({'opacity':0});
				}
				floor_popup.css({'display':'none'});
			}
		});
		test_floors(frame2);
		content_move();				
	}
	function test_floors(frame2) {
		var rooms_av=[];
		frame2.find('.rooms_sel.active').each(function(){
			rooms_av.push(Number($(this).data('targ')));	
		})
		if(rooms_av.length==0) {
			rooms_av=[1,2,3];
		}
		paper_size.forEach(function(el){
			var alt=el.data('alt');			
			var av=0;
			var floor_data=data.floors[plans_val['b']+'-'+alt];
			if(floor_data) {
				$.each(rooms_av, function(index, value) {
					av+=floor_data.arc[value];
				})
			}
			if(av!=0) {
				el.attr({'opacity':0.4,'cursor':'pointer'}).data('av',av);
			} else {
				el.attr({'opacity':0,'cursor':'default'}).data('av',av);
			}
		})
	}
	function open_floor_popup(alt) {
		plans_val['s']=Number(alt.split('-')[0]);
		plans_val['f']=Number(alt.split('-')[1]);
		plans_val['floor_frame'].stop(true).css({'display':'block','opacity':0}).transition({'opacity':1},700);
		show_blur_bg();
		load_floor(0);
		$('.korp_det.n1').addClass('small');
	}	
	function close_floor() {
		plans_val['floor_frame'].stop(true).transition({'opacity':0},400,function(){
			$(this).hide().find('.plan_frame_centrer').html('');
			paper_size2=null;
			paper_size3=null;
			paper_size4=null;				
		})
		hide_blur_bg();
		$('.korp_det.n1').removeClass('small');
		body_size.load_content('/plans/korpus'+plans_val['b']+'/section'+plans_val['s'],{'suppress_load':true});
	}
	function show_blur_bg(search_bg) {
		$('.blur_bg').stop(true).css({'display':'block','opacity':0}).transition({'opacity':1},400);
		if(search_bg) {
			$('.korp_det.n0').addClass('low-z');
		}
	}
	function hide_blur_bg(search_bg) {
		$('.blur_bg').stop(true).transition({'opacity':0},700,function(){
			$(this).hide();	
			if(search_bg) {
				$('.korp_det.n0').removeClass('low-z');
			}
		});		
	}
	function load_floor_details() {
		plans_val['floor_frame'].find('.floor_down, .floor_up').addClass('active');		
		if(!data.floors[plans_val['b']+'-'+plans_val['s']+'-'+(plans_val['f']-1)]) {
			plans_val['floor_sel'].find('.floor_down').removeClass('active');
		};
		if(!data.floors[plans_val['b']+'-'+plans_val['s']+'-'+(plans_val['f']+1)]) {
			plans_val['floor_sel'].find('.floor_up').removeClass('active');
		};
		plans_val['floor_frame']	
			.find('.windrose').attr('src','/assets/images/minimaps/'+plans_val['b']+'-'+plans_val['s']+'.png');
		plans_val['floor_sel'].find('.korp_det_val>div').text(plans_val['f']);		
	}
	function load_floor(time,no_history) {
		load_floor_details();
		plans_val['floor_frame']			
			.find('.plan_frame_centrer').stop(true).transition({'opacity':0},time,function(){
				load_floor_map(time);
				plans_val['floor_frame']
					.find('.at_floor').stop(true).fadeIn(time*2).end()
					.find('.at_apart, .apart_details_frame').stop(true).fadeOut(time*2);
			});
		if(!no_history) {
			body_size.load_content('/plans/korpus'+plans_val['b']+'/section'+plans_val['s']+'/floor'+plans_val['f'],{'suppress_load':true});
		}
	}
	function rc_attr(param) {
		return {'opacity':0.2};
	}
	function load_floor_map(time) {
		$('.plans_close').attr('class','plans_close floor_close');
		paper_size2=null;
		paper_size3=null;
		paper_size4=true;
		plans_val['n']=null;
		var fr=plans_val['floor_frame'].find('.plan_frame_centrer')
		fr.load('/assets/pages/floors/'+plans_val['b']+'-'+plans_val['s']+'-'+plans_val['f']+'.html',function(){
			fr.find('.floor_map').load(function(){
				$(this).unbind('load');
				fr.transition({'opacity':1},time);
			})
			fr.find('.floor_map_cont').clone().appendTo($(this));			
			var zoom;
			fr.find('.floor_map:first').attr('src','/assets/i/blank.gif');			
			paper_size2=fr.find('.floor_map:first').area2svg({
				'opacity':0,
				'fill':'#f29228',
				'fill-opacity':1,
				'stroke-opacity':0,
				onclick:function(el) {
				},
				onmouseover:function(el) {
				},
				onmouseout:function(el) {
				}
			})
			paper_size3=fr.find('.floor_map:last').area2svg({
				'opacity':0,
				'cursor':'default',
				onclick:function(el) {
					var alt=el.data('alt');
					if(el.data('av')==1) {							
						load_apart(alt,200);
					}
				},
				onmouseover:function(el) {	
					var alt=el.data('alt');
					if(paper_size2) {
						if(el.data('av')==1) {			
							paper_size2.getByAlt(alt).attr({'opacity':0.6});
						}
						load_apart_details(alt);
					}
					if(data.apartments[alt].st==1) {
						var box=el.getBBox();
						var scale=el.paper.width/fr.find('.floor_map').attr('width');
						zoom.css({'display':'block','top':box.cy*scale,'left':box.cx*scale});
					}
					
				},
				onmouseout:function(el) {
					var alt=el.data('alt');
					if(paper_size2 && el.data('av')==1) {
						paper_size2.getByAlt(alt).attr(rc_attr(data.apartments[alt].rc));
					}
					if(!plans_val['n']) {
						plans_val['apart_details'].css({'display':'none'});
					}
					zoom.css({'display':'none'});
				},
				each:function(el) {
					var alt=el.data('alt');
					var d=data.apartments[alt];
					if(!d || d.st!=1) {
						if(!d) {
							d={};
							d.st=0;	
							console.log('null data at '+alt)
						}
						if(d.st==2) {
							paper_size2.getByAlt(alt).attr({'opacity':0.15,'fill':'#000000'});
						} else {
							paper_size2.getByAlt(alt).attr({'opacity':0});
						}
					} else {
						paper_size2.getByAlt(alt).attr(rc_attr(data.apartments[alt].rc));
						el.attr({'cursor':'pointer'});
					}
					el.data('av',d.st);
				}
			})
			fr.find('.floor_map:last').after('<div class="floor_zoom"></div>');
			zoom=$(this).find('.floor_zoom');
			plan_check_size();
		})
	}
	function load_apart_details(alt) {
		var d=data.apartments[alt];
		var sale_text='РЅРµ РІ РїСЂРѕРґР°Р¶Рµ';
		if(d.st==1) {
			sale_text='РІ РїСЂРѕРґР°Р¶Рµ';
		} else
		if(d.st==2) {
			sale_text='Р·Р°Р±СЂРѕРЅРёСЂРѕРІР°РЅР°';
		} else
		if(d.st==0) {
			sale_text='РїСЂРѕРґР°РЅР°';
		}
		plans_val['apart_details'].stop(true).css({'display':'block','opacity':1})
			.find('.n1 .val').text(d.n).end()
			.find('.n5 .val').text(sale_text);
		if(d.st==1) {
			var rc=d.rc;
			if(rc!=0) {
				var rc_text='<div class="val bold">'+rc+'</div>РєРѕР»РёС‡РµСЃС‚РІРѕ РєРѕРјРЅР°С‚';
			} else {
				var rc_text='<div class="val bold is_st">СЃС‚СѓРґРёСЏ</div>';
			}
			plans_val['apart_details'].removeClass('not-sale')				
				.find('.n2').html(rc_text).end()
				.find('.n3 .val').html(d.sq).end()
				.find('.n4 .val').text(addspace(d.tc));
		} else {
			plans_val['apart_details'].addClass('not-sale');
		}
	}
	function load_apart(alt,time,no_history) {
		plans_val['n']=alt;
		paper_size2=null;
		paper_size3=null;
		plans_val['floor_frame'].find('.plan_frame_centrer').stop(true).transition({'opacity':0},time,function(){
			$(this).html('<img class="apart_img" src="/assets/images/apts/'+alt+'.png" />')
				.find('.apart_img').load(function(){
					$(this).unbind('load').parent().transition({'opacity':1},time);					
				})
			})
		plans_val['floor_frame']
			.find('.at_floor').stop(true).fadeOut(time*2).end()
			.find('.at_apart').stop(true).fadeIn(time*2).end()
			.find('.pdf_btn').attr('href','/assets/php/pdf.php?id='+alt);
		$('.plans_close').attr('class','plans_close apart_close');
		if(!no_history) {
			body_size.load_content('/plans/korpus'+plans_val['b']+'/section'+plans_val['s']+'/floor'+plans_val['f']+'/flat'+data.apartments[plans_val['n']].n,{'suppress_load':true});
		}
	}
	
	/* РєР°СЂС‚С‹ */
	
	function load_placement_map(placement) {
		//console.log(xml_txt)
		var obj_point = new google.maps.LatLng(55.046928,82.926279);
		var off_point = new google.maps.LatLng(55.046928,82.929522);
		var center_point = new google.maps.LatLng(55.0431,82.9252);
		var myOptions = {
			zoom: 15,
			center: center_point,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			disableDefaultUI: true,
			zoomControl: true,
			zoomControlOptions: {
			  style: google.maps.ZoomControlStyle.LARGE,
			  position: google.maps.ControlPosition.RIGHT_CENTER
			}			
		};
		
		var map = new google.maps.Map(document.getElementById('map_place'),myOptions);
		
		var colors=['#4fa2db',null,null,'#c24894','#f8b000','#f29229',null,'#009fe2','#4e2483','#2377c4','#ff0013','#2db3a0','#e20613'];
		var image1 = new google.maps.MarkerImage(
			'/assets/i/sprite.png',
			new google.maps.Size(57,73),
			new google.maps.Point(804,5),
			new google.maps.Point(25,73)
		);
		var marker1 = new MarkerWithLabel({
			position: obj_point,
			title: 'РћРіРЅРё РЎРёР±РёСЂРё',
			map: map,
			icon: image1,
			labelContent: '<span class="gmaps_labels_span n5" style="background-color:'+colors[5]+'">РћРіРЅРё РЎРёР±РёСЂРё</span>',
			labelAnchor: new google.maps.Point(90,-8),
			labelClass: 'gmaps_labels',
			labelVisible: false,
		});
		google.maps.event.addListener(marker1,'mouseover',function() {
			this.setOptions({labelVisible:true,zIndex:10000});
		});
		google.maps.event.addListener(marker1,'mouseout',function() {
			this.setOptions({labelVisible:false,zIndex:null});
		});	
		var image_route = new google.maps.MarkerImage(
			'/assets/i/sprite.png',
			new google.maps.Size(56,73),
			new google.maps.Point(929,155),
			new google.maps.Point(25,73)
		);
		var route_marker = new google.maps.Marker({
			position: obj_point,
			map: map,
			icon: image_route,
			clickable: false,
			visible: false
		});		
		var path;
		var markers=[];
		var lineSymbol1 = {
			path: 'M 0,-1 0,-0.99',
			strokeOpacity: 1,
			strokeColor: '#f36d21',
			scale: 6
		};
		var directionsDisplay = new google.maps.DirectionsRenderer({map:null,suppressMarkers:true,polylineOptions:{strokeOpacity:0,
			icons: [{
				icon: lineSymbol1,
				offset: '0',
				repeat: '15px'
			}]
		}});
		var directionsService = new google.maps.DirectionsService();	
		function calcRoute(position) {
			var request = {
				origin: position, 
				destination: obj_point,
				travelMode: google.maps.DirectionsTravelMode.DRIVING
			};			
			directionsService.route(request, function(response, status) {
				if (status == google.maps.DirectionsStatus.OK) {
					directionsDisplay.setDirections(response);					
				}			
			});
		}	
		function placeMarker(position) {
			route_marker.setOptions({position:position,visible:true,animation:google.maps.Animation.DROP});
			directionsDisplay.setMap(map);
			calcRoute(position);
			$('.route_help').trigger('click');
		}	
		google.maps.event.addListener(map, 'click', function(e) {
			if(path) {
				placeMarker(e.latLng);
			}
		});
		$('.open_route').bind('click',function(){
			path=true;
			map.panTo(center_point);
			map.setZoom(11);
			$('.route_adr_frame').stop(true).fadeIn(400)
				.find('.route_adr_input').val('').placeholder();
			for(var i=0; i<markers.length; i++) {
				markers[i].setVisible(setVisible(false,markers[i].type));
			}
			$('.open_route').fadeOut(300);
		});
		$('.route_adr_close').bind('click',function(){
			path=false;
			map.panTo(center_point);
			map.setZoom(15);
			$('.route_adr_frame').stop(true).fadeOut(300);
			directionsDisplay.setMap(null);
			route_marker.setOptions({visible:false});
			for(var i=0; i<markers.length; i++) {
				markers[i].setVisible(setVisible(true,markers[i].type));
			}
			$('.open_route').fadeIn(400);
		});
		$('.route_adr_btn').bind('click', function() {
			var address = $(this).siblings('.route_adr_input').val();
			if(address != '' && address != 'Р’С‹Р±РµСЂРёС‚Рµ С‚РѕС‡РєСѓ РЅР° РєР°СЂС‚Рµ РёР»Рё РІРІРµРґРёС‚Рµ Р°РґСЂРµСЃ РЅР°С‡Р°Р»Р° РјР°СЂС€СЂСѓС‚Р°') {
				$.ajax({
					url: 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address,
					dataType: 'json',
					success: function(response){
						if(response.results.length) {
							inputCoords = new google.maps.LatLng(response.results[0].geometry.location.lat, response.results[0].geometry.location.lng);
							placeMarker(inputCoords);
						}
					}
				});
			}
		});
		$('.route_adr_input').bind('keydown',function(e){
			var key=e.which;
			if(key==13 && path==true) {
				$('.route_adr_btn').trigger('click');	
			};
		})
		if(placement) {
			function get_icon_style(type) {
				var img;
				if(type==10) {
					img=new google.maps.MarkerImage(
						'/assets/i/sprite.png',
						new google.maps.Size(32,32),
						new google.maps.Point(804,88),
						new google.maps.Point(16,32)
					)
				} else
				if(type==9) {
					img=new google.maps.MarkerImage(
						'/assets/i/sprite.png',
						new google.maps.Size(32,32),
						new google.maps.Point(838,88),
						new google.maps.Point(16,32)
					)
				} else
				if(type==5) {
					img=new google.maps.MarkerImage(
						'/assets/i/sprite.png',
						new google.maps.Size(57,73),
						new google.maps.Point(804,5),
						new google.maps.Point(25,73)
					)
				} else {
					img=new google.maps.MarkerImage(
						'/assets/i/sprite.png',
						new google.maps.Size(47,61),
						new google.maps.Point(type*50+348,246),
						new google.maps.Point(24,61)
					)
				}
				return img;
			}
			function setVisible(state,type) {
				return state;
			}
			//var markers=[[],[],[],[],[],[],[],[],[],[],[],[],[]];
			
			for (var i=0; i<xml_txt.length; i++) {
				var type=Number(xml_txt[i][4]);
				var marker = new MarkerWithLabel({
					position: new google.maps.LatLng(xml_txt[i][0],xml_txt[i][1]),
					title: xml_txt[i][2],
					map: map,
					icon: get_icon_style(type),
					type: type,
					labelContent: '<span class="gmaps_labels_span n'+type+'" style="background-color:'+colors[type]+'">'+xml_txt[i][2]+'</span>',
					labelAnchor: new google.maps.Point(90,-8),
					labelClass: 'gmaps_labels',
					labelVisible: false,
					visible: setVisible(true,type)
				});
				//markers[type].push(marker);
				markers.push(marker);
				google.maps.event.addListener(marker,'mouseover',function() {
					this.setOptions({labelVisible:true,zIndex:10000});
				});
				google.maps.event.addListener(marker,'mouseout',function() {
					this.setOptions({labelVisible:false,zIndex:null});
				});
			}
		} else {
			var marker2 = new MarkerWithLabel({
				position: off_point,
				title: 'РћС„РёСЃ РїСЂРѕРґР°Р¶',
				map: map,
				icon: image1,
				labelContent: '<span class="gmaps_labels_span n5" style="background-color:'+colors[5]+'">РћС„РёСЃ РїСЂРѕРґР°Р¶</span>',
				labelAnchor: new google.maps.Point(90,-8),
				labelClass: 'gmaps_labels',
				labelVisible: false,
			});
			google.maps.event.addListener(marker2,'mouseover',function() {
				this.setOptions({labelVisible:true,zIndex:10000});
			});
			google.maps.event.addListener(marker2,'mouseout',function() {
				this.setOptions({labelVisible:false,zIndex:null});
			});	
		}
	}
	
	
	
	body_size.pjax2({
		beforeSend:function(new_page) {
			preloader.show();
			unload_js(old_page,new_page);
			var video_name=old_page+'-'+new_page;
			if(ani) {
				clearInterval(videoInt);
				clearTimeout(videoTime);
				bg_video.pause();
			}
			if(ani && $.inArray(video_name,ani_names)!=-1) {
				is_video=true;
				var i=0;
				html_loaded=false;
				$('#bg_video').show();
				bg_video.src([				
					{type:'video/webm', src:'/assets/video/fly/'+video_name+'.webm'},
					{type:'video/mp4', src:'/assets/video/fly/'+video_name+'.mp4'}
				]);
				scroll1.find('.'+old_page+'_frame').children(':not(.bg_img)').stop(true).transition({'opacity':0},time,function(){
					$(this).hide();
				})
				scroll2.find('.'+old_page+'_frame').stop(true).transition({'opacity':0},time,function(){
					$(this).remove();
				})
				videoInt=setInterval(function(){
					i++;
					if ((bg_video.bufferedPercent()>0.60 || i>100) && html_loaded) {
						clearInterval(videoInt);
						preloader.hide();
						bg_video.play();
						setTimeout(function(){
							scroll1.find('.'+old_page+'_frame>.bg_img:first').hide();
						},90);
					}
				},50);
			} else {
				is_video=false;
			}
		},
		success:function(data,type,url,slug,custom_type) {
			//console.log(type,url,slug,custom_type)
			page=type;
			var txt1=/to_centrer1">([\s\S]*)<\/div>\s*<div class="to_centrer2/.exec(data)[1];
			var txt2=/to_centrer2">([\s\S]*)<\/div>\s*$/.exec(data)[1];
			if(mobile==true) {
				//txt1=txt1.replace(/(bg_img"\s.*)\/(\w*\.jpg"\s\/>)/,'$1/m/$2');
			}
			scroll1.append('<div class="load_frame '+type+'_frame'+custom_type+'" style="left:100%;">'+txt1+'</div>');
			scroll2.append('<div class="load_frame '+type+'_frame'+custom_type+'" style="left:100%;">'+txt2+'</div>');
			$('.load_frame.'+type+'_frame:last-child .bg_img:first').load(function(){
				$(this).unbind('load');
				if(is_video) {
					html_loaded=true;
				} else {
					preloader.hide();	
					load_js(type,old_page);
				}
			})
			add_stat(url);
		},
		success_function:load_js,
	});

 load_js("home");


		
	body_size.bind('click',function(e){
		var targ_id=e.target.id;
		var targ_class=e.target.className;
		var targ=$(e.target);
		var targ_data=targ.data('targ');
		
		if(targ_class=='open_video') {
			open_popup_video(targ_data);
		} else
		if(targ_class=='close_btn video_close') {
			centrer2.find('.video_popup').stop(true).transition({'opacity':0},300,function(){
				unload_popup_video();
				$(this).remove();
			})
		} else
		if(targ_class=='ani_toggle') {
			if(ani) {
				ani=false;				
				targ.text('РІРєР»СЋС‡РёС‚СЊ Р°РЅРёРјР°С†РёСЋ');			
			} else {
				ani=true;
				targ.text('РІС‹РєР»СЋС‡РёС‚СЊ Р°РЅРёРјР°С†РёСЋ');
			};
		} else
		if(targ_class=='pano_open_btn') {
			load_pano(targ_data);
		} else
		if(targ_class=='close_btn pano_close') {
			unload_pano();
		} else
		if(targ_class=='open_feedback n0' || targ_class=='open_feedback n1' || targ_class=='buy_icons n1') {
			load_feedback(targ_data);
		} else
		if(targ_class=='close_btn feedback_close') {
			centrer2.find('.feedback_bg').stop(true).transition({'opacity':0},300,function(){
				$(this).parent().transition({'opacity':0},400,function(){
					$(this).remove();
				})
			});
		} else
		if(targ_class=='send_btn') {
			var fr=targ.parents('.feedback_bg');
			fr.find('input, textarea').each(function(){
				var val=$(this).val();
				if(val=='' || val==$(this).attr('placeholder') || ($(this).hasClass('mail_input') && !/\S+@\S+\.\S+/.test(val))) {
					$(this).addClass('error');
				}
			})
			if(fr.hasClass('n2')) {
				if(!fr.find('.phone_input').hasClass('error') || !fr.find('.mail_input').hasClass('error')) {
					fr.find('.phone_input, .mail_input').removeClass('error')	
				}
			}
			if(!fr.find('.error').length) {
				var txt_name=fr.find('.feedback_input1').val();
				var txt_text='';
				var txt_title=fr.find('.feedback_name').data('title');
				targ.addClass('no-active');
				$.cookie('name',$.md5(txt_name),{expires:1,path:'/'});
				fr.find('input, textarea').each(function(){
					txt_text+=$(this).data('title')+': '+$(this).val()+'\n';
				})
				$.ajax({
					url: '/assets/pages/mail.php',
					cache: false,
					type: 'POST',
					data: 'name='+txt_name+'&text='+txt_text+'&title='+txt_title,
					success: function(data){
						if(data=='ok') {
							fr
								.find('.feedback_inputs').fadeOut(200).end()
								.find('.feedback_sended').delay(200).fadeIn(200).delay(5000).fadeOut(200,function(){
									fr.find('.feedback_close').trigger('click');
								});	
						} else {
							targ.removeClass('no-active');
						}
					},
					error: function(){
						targ.removeClass('no-active');
					}
				});				
			}
		} else		
		if(targ_class=='rotate_help' || targ_class=='rotate_help_frame') {
			rotate_help.hide();			
			rotate_help=null;
		} else
		if(targ_class=='carousel_arrow left') {
			$('.carousel_dots.active').prev().trigger('click');
		} else
		if(targ_class=='carousel_arrow right') {
			$('.carousel_dots.active').next().trigger('click');
		} else
		if(targ_class=='carousel_dots' && carousel_scroll_av) {
			carousel_scroll_av=false;
			var prev_btn=$('.carousel_arrow.left');
			var next_btn=$('.carousel_arrow.right');
			targ.addClass('active').siblings('.active').removeClass('active');
			if(targ.is(':last-child')) {
				prev_btn.show();
				next_btn.hide();
			} else
			if(targ.is(':first-child')) {
				prev_btn.hide();
				next_btn.show();
			} else {
				prev_btn.show();
				next_btn.show();
			}
			var time=600;
			var targ_pos=-100*1/carousel_items_targ*Math.min(targ_data*carousel_items_targ, carousel_items_num-carousel_items_targ)+'%';
			carousel_line.stop(true).transition({'left':targ_pos},time,easyInOut,function(){
				carousel_scroll_av=true;
			});
			if(page=='construction') {
				make_constr_active(targ_data,time);
			}
		} else 
		if(targ_class=='plans_close floor_close') {
			close_floor();
		} else 
		if(targ_class=='plans_close apart_close') {
			load_floor(200);
		} else
		if(targ_class=='floor_down active') {
			plans_val['f']--;
			load_floor(100);
		} else
		if(targ_class=='floor_up active') {
			plans_val['f']++;
			load_floor(100);
		} else
		if(targ_class=='rooms_sel' || targ_class=='rooms_sel active') {
			targ.toggleClass('active').siblings('.active').removeClass('active');			
			test_floors(scroll2.find('.korpus_frame:last'));
		} else
		if(targ_class=='bg_slides_arrow left' || targ_class=='bg_slides_arrow right') {
			load_bg_slide(bg_slides['num']+targ_data);
		} else
		if(targ_class=='bg_slides_dots') {
			load_bg_slide(targ_data);
		} else
		if(targ_class=='adv_str up') {
			var next_targ=$('.adv_icons.active').prev('.adv_icons');
			if(!next_targ.length) {
				next_targ=$('.adv_icons:last');
			}
			next_targ.trigger('click');
		} else
		if(targ_class=='adv_str down') {
			var next_targ=$('.adv_icons.active').next('.adv_icons');
			if(!next_targ.length) {
				next_targ=$('.adv_icons:first');
			}
			next_targ.trigger('click');
		} else
		if(targ_class=='road_sel n1' || targ_class=='road_sel n2' || targ_class=='road_sel n3' || targ_class=='road_sel n4') {
			load_road_text(targ_data,200);			
		} else
		if(targ_class=='close_btn close_road') {
			targ.parent().transitionStop().transition({'opacity':0},400,function(){
				$(this).hide();	
			})
		} else
		if(targ_class=='open_road') {
			$('.road_frame').transitionStop().css({'opacity':0,'display':'block'}).transition({'opacity':1},500);
			load_road_text(1,0);
		}
	})
	.bind('mousemove',function(e){
		mouse_pos=e;
		move_bg();
	})
	
	$('.construction_fancy').on('click',function(){
		if($(this).hasClass('active')) {
			var arr=[];
			var url=$(this).data('url');
			var path=$(this).data('path');
			$.ajax({
				url: url,
				dataType: 'json',
				success: function(response){
					var arr=[];
					for(var i=0; i<response.length; i++) {
						arr.push({href:path+'/'+response[i], title:response[i].replace(/\.\w+$/,'')});
					}
					$.fancybox(arr,{type:'image',padding:0,margin:[75,100,40,100]});
					//console.log(arr);	
					arr=null;
				}
			});
		} else {
			$('.carousel_dots').eq($(this).index('.carousel_item')).trigger('click');
		}
    })	
	$('.adv_icons').on('click',function(){
		if(!$(this).hasClass('active')) {			
			adv_pos($(this).data('targ'),1000);
		}
	})
	$('.plans_frame .korpus_icon')
		.on('mouseenter',function(){
			var alt=$(this).data('targ');
			if(paper_size) {
				paper_size.getByAlt(alt).attr({'opacity':0.4});
			}
		})
		.on('mouseleave',function(){
			var alt=$(this).data('targ');
			if(paper_size) {
				paper_size.getByAlt(alt).attr({'opacity':0});
			}
		})
});